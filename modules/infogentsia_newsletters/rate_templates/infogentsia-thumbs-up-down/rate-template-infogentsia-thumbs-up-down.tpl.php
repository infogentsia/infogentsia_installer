<?php

/**
 * @file
 * Rate widget theme
 */
?>

<?php
  /*
  <div class="rate-label">
    <?php print $display_options['title']; ?>
  </div>
  */
?>

<ul>
  <li class="thumb-up">
    <?php print $up_button; ?>
  </li>
  <li class="thumb-down">
    <?php print $down_button; ?>
  </li>
</ul>
<?php

if ($display_options['description']) {
  print '<div class="rate-description">' . $display_options['description'] . '</div>';
}
