<?php

function infogentsia_newsletters_settings() {
/*
  $voptions = array('' => t('-- Select --'));
  $noptions = array('' => t('-- Select --'));

  $vocabularies = taxonomy_get_vocabularies();
  foreach($vocabularies as $vocabulary) {
    $voptions[$vocabulary->vid] = t($vocabulary->name);
  }

  $types = node_type_get_types();
  foreach($types as $node_type) {
    $noptions[$node_type->type] = t($node_type->name);
  }*/
  $defaultList = NULL;
  $lists = array('' => t('-- Select --'));
  foreach (mailchimp_get_lists() as $item) {
    $lists[$item['id']] = $item['name'];
    if (is_null($defaultList)) $defaultList = $item['id'];
  }
  
  $form['infogentsia_newsletters_campaign_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Campaign name'),
    '#description' => t('The name of the campaign.'),
    '#default_value' => variable_get('infogentsia_newsletters_campaign_name',variable_get('site_name','Infogentsia')),
    '#maxlength' => 72,
    '#required' => TRUE,
    '#size' => 32
  );
 
  // Newsletter topics
  /*
  $form['infogentsia_newsletters_vocabulary'] = array(
    '#type' => 'select',
    '#title' => t('Vocabulary for Topics'),
    '#options' => $voptions,
    '#default_value' => variable_get('infogentsia_newsletters_vocabulary', ''),
    '#description' => t('What taxonomy vocabulary are you using for the Interest Groups')
  );
  */
  // Newsletter list
  $form['infogentsia_newsletters_list'] = array(
    '#type' => 'select',
    '#title' => t('List'),
    '#options' => $lists,
    '#default_value' => variable_get('infogentsia_newsletters_list', $defaultList),
    '#description' => t('What MailChimp list will the campaigns and interest groups be created in. If no lists show, create one on the Lists and Users tab (/admin/config/services/mailchimp/lists).'),
    '#ajax' => array(
      'event' => 'change',
      'wrapper' => 'nil-group',
      'callback' => 'infogentsia_newsletter_populate_interest_groupings',
      'method' => 'replace',
    )
  );

  $mcapi = mailchimp_get_api_object();
  // Get interest groups
  $interestGroupings = array();
  $defaultigl = NULL;
  if (variable_get('infogentsia_newsletters_list', $defaultList) != '') {
    $interestGroupings = $mcapi->lists->interestGroupings(variable_get('infogentsia_newsletters_list', $defaultList));   
  }
  $options = array();
  if (is_array($interestGroupings)) {
    foreach($interestGroupings as $interestGrouping) {
      $options[$interestGrouping['id']] = $interestGrouping['name'];
      if (is_null($defaultigl)) $defaultigl = $interestGrouping['id'];
    }
  }
  $form['infogentsia_newsletters_interest_list_group'] = array(
    '#type' => 'select',
    '#title' => t('Interest Grouping'),
    '#options' => $options,
    '#default_value' => variable_get('infogentsia_newsletters_interest_list_group',$defaultigl),
    '#prefix' => '<div class="squadron-infogentsia_newsletters_interest_list_group" id="nil-group">', 
    '#suffix' => '</div>',
    '#description' => t('Select an existing interest grouping from MailChimp. This grouping needs to be created within MailChimp and have a group of "All Topics" created manually. All other groups will be maintained through this interface.')
  );  
  
  //array analytics optional - if provided, use a struct with "service type" as a key and the "service tag" as a value. Use  "google" for Google Analytics, "clicktale" for ClickTale, and "gooal" for Goo.al tracking. The "tag" is any custom text (up to 50  characters) that should be included.
  $form['infogentsia_newsletters_analytics_group'] = array(
    '#type' => 'fieldset',
    '#title' => t('Analytics')
  );
  $analytics = array('google'=>t('Google Analytics'), 'clicktale'=>t('ClickTale'), 'gooal'=>t('Goo.al'));
  $form['infogentsia_newsletters_analytics_group']['infogentsia_newsletters_analytics'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Services'),
    '#options' => $analytics,
    '#default_value' => variable_get('infogentsia_newsletters_analytics', array())
  );
  foreach ($analytics as $key=>$value) {
    $form['infogentsia_newsletters_analytics_group']['infogentsia_newsletters_' . $key] = array(
      '#type' => 'textfield',
      '#title' => t('Code for ' . $value),
      '#description' => t('Enter the tracking code for @e.', array('@e' => $value)),
      '#default_value' => variable_get('infogentsia_newsletters_' . $key, ''),
      '#maxlength' => 50,
      '#size' => 50,
      '#states' => array(
        'visible' => array(
          ':input[name="infogentsia_newsletters_analytics[' . $key . ']"]' => array('checked' => TRUE)
        )
      )
    );
  }
  
  $form['infogentsia_newsletters_tracking_group'] = array(
    '#type' => 'fieldset',
    '#title' => t('Tracking')
  );
  $tracking = array('opens'=>t('Opens'), 'html_clicks'=>t('HTML Clicks'), 'text_clicks'=>t('Text Clicks'));
  foreach ($tracking as $key=>$value) {
    $form['infogentsia_newsletters_tracking_group']['infogentsia_newsletters_' . $key] = array(
      '#type' => 'checkbox',
      '#title' => t('Track ' . $value),
      '#description' => t('Enter the tracking code for @e.', array('@e' => $value)),
      '#default_value' => variable_get('infogentsia_newsletters_' . $key, TRUE),
    );
    // Click tracking must be TRUE on free accounts.
    if ($key != 'opens') $form['infogentsia_newsletters_tracking_group']['infogentsia_newsletters_' . $key]['#disabled'] = TRUE;
  }
  
  //boolean authenticate optional - set to true to enable SenderID, DomainKeys, and DKIM authentication, defaults to false.
  $form['infogentsia_newsletters_authenticate'] = array(
    '#type' => 'checkbox',
    '#title' => t('Authenticate'),
    '#default_value' => variable_get('infogentsia_newsletters_authenticate', FALSE),
    '#description' => t('Authentication is sort of like a license plate for your email. It provides a trackable identifier which indicates you\'re probably legit. Note, this requires additional configuration within MailChimp.')
  );
  
  //boolean auto_footer optional - Whether or not we should auto-generate the footer for your content. Mostly useful for content from URLs or Imports 
  $form['infogentsia_newsletters_auto_footer'] = array(
    '#type' => 'checkbox',
    '#title' => t('Auto-footer'),
    '#default_value' => variable_get('infogentsia_newsletters_auto_footer', FALSE),
    '#description' => t('Whether or not we should auto-generate the footer for your content. Mostly useful for content from URLs or Imports.')
  );
  
  //boolean inline_css optional - Whether or not css should be automatically inlined when this campaign is sent, defaults to  false.
  $form['infogentsia_newsletters_inline_css'] = array(
    '#type' => 'checkbox',
    '#title' => t('Inline CSS'),
    '#default_value' => variable_get('infogentsia_newsletters_inline_css', TRUE),
    '#description' => t('Whether or not css should be automatically inlined when this campaign is sent, defaults to true.')
  ); 
  
  //boolean generate_text optional - Whether of not to auto-generate your Text content from the HTML content. Note that this will be ignored if the Text part of the content passed is not empty, defaults to false.
  $form['infogentsia_newsletters_generate_text'] = array(
    '#type' => 'checkbox',
    '#title' => t('Generate Text'),
    '#default_value' => variable_get('infogentsia_newsletters_generate_text', TRUE),
    '#description' => t('Whether of not to auto-generate your Text content from the HTML content. Note that this will be ignored if the Text part of the content passed is not empty, defaults to true.')
  ); 
  
  $form['infogentsia_newsletters_content'] = array(
    '#type' => 'fieldset',
    '#title' => t('Content')
  );
  
  $form['infogentsia_newsletters_content']['infogentsia_newsletters_rsstags'] = array(
    '#markup' => t('<p>MailChimp has it\'s own placeholder tags (*|SOMETHING|*) that you can use in the subject, the content, etc. For a complete list of the RSS merge tags, please vist <a href="http://kb.mailchimp.com/merge-tags/rss-merge-tags">http://kb.mailchimp.com/merge-tags/rss-merge-tags</a></p>')
  );
  $form['infogentsia_newsletters_content']['infogentsia_newsletters_alltags'] = array(
    '#markup' => t('<p>MailChimp also has quite a few placeholder tags for use for personalization, list information, etc. For a complete list of ALL merge tags, please vist <a href="http://kb.mailchimp.com/merge-tags/all-the-merge-tags-cheatsheet">http://kb.mailchimp.com/merge-tags/all-the-merge-tags-cheatsheet</a></p>')
  );
   
  // Add campaign settings
  //string subject
  $form['infogentsia_newsletters_content']['infogentsia_newsletters_subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Subject'),
    '#description' => t('The subject of the email you send out.'),
    '#default_value' => variable_get('infogentsia_newsletters_subject','News Briefings from ' . variable_get('site_name','Infogentsia') . ' for *|RSSFEED:DATE|*'),
    '#maxlength' => 72,
    '#required' => TRUE,
    '#size' => 32
  );
  //string from_email the From: email address for your campaign message
  $form['infogentsia_newsletters_content']['infogentsia_newsletters_from_email'] = array(
    '#type' => 'textfield',
    '#title' => t('From email'),
    '#description' => t('The From: email address for your campaign message.'),
    '#default_value' => variable_get('infogentsia_newsletters_from_email',variable_get('site_mail','')),
    '#maxlength' => 255,
    '#required' => TRUE,
    '#size' => 32
  );
  //string from_name the From: name for your campaign message (not an email address)
  $form['infogentsia_newsletters_content']['infogentsia_newsletters_from_name'] = array(
    '#type' => 'textfield',
    '#title' => t('From name'),
    '#description' => t('The From: name for your campaign message (not an email address).'),
    '#default_value' => variable_get('infogentsia_newsletters_from_name',variable_get('site_name','')),
    '#maxlength' => 255,
    '#required' => TRUE,
    '#size' => 32
  );
   
  //string to_name the To: name recipients will see (not email address)
  $form['infogentsia_newsletters_content']['infogentsia_newsletters_to_name'] = array(
    '#type' => 'textfield',
    '#title' => t('To name'),
    '#description' => t('The To: name name recipients will see (not an email address).'),
    '#default_value' => variable_get('infogentsia_newsletters_to_name','*|FNAME|*'),
    '#maxlength' => 255,
    '#required' => TRUE,
    '#size' => 32
  );
  $mcapi = mailchimp_get_api_object();
  if ($mcapi) {
    $types = array('user'=>true, 'gallery'=>true, 'base'=>true);
    $default_user_template_id = NULL;
    $default_gallery_template_id = NULL;
    try{
      $retval = $mcapi->templates->getList($types);
      $form['infogentsia_newsletters_content']['infogentsia_newsletters_template_type'] = array(
        '#type' => 'select',
        '#title' => t('Template Type'),
        '#options' => array('user'=>t('User'), 'gallery'=>t('Gallery')),
        '#default_value' => variable_get('infogentsia_newsletters_template_type','gallery'),
        '#description' => t('Which type of template will you use?')
      );

      //int template_id optional - use this user-created template to generate the HTML content of the campaign (takes precendence over  other template options)
      $user_templates = array();
      foreach($retval['user'] as $tmpl){
        $user_templates[$tmpl['id']] = $tmpl['name'];
        if (is_null($default_user_template_id)) $default_user_template_id = $tmpl['id'];
      }
      $form['infogentsia_newsletters_content']['infogentsia_newsletters_template_id'] = array(
        '#type' => 'select',
        '#title' => t('User Template'),
        '#options' => $user_templates,
        '#description' => t('Select a user template to use.'),
        '#default_value' => variable_get('infogentsia_newsletters_template_id',$default_user_template_id),
        '#states' => array(
          'visible' => array(
            ':input[name="infogentsia_newsletters_template_type"]' => array('value' => 'user')
          )
        ),
        '#ajax' => array(
          'callback' => 'infogentsia_newsletters_template_callback',
          'method' => 'replace',
          'wrapper' => 'content-sections',
        ),
      );
        

      //int gallery_template_id optional - use a template from the public gallery to generate the HTML content of the campaign (takes  precendence over base template options)
      $gallery_templates = array();
      foreach($retval['gallery'] as $tmpl){
        $gallery_templates[$tmpl['id']] = $tmpl['name'];
        if (is_null($default_gallery_template_id)) $default_gallery_template_id = $tmpl['id'];
      }
      $form['infogentsia_newsletters_content']['infogentsia_newsletters_gallery_template_id'] = array(
        '#type' => 'select',
        '#title' => t('Gallery Template'),
        '#options' => $gallery_templates,
        '#description' => t('Select a gallery template to use.'),
        '#default_value' => variable_get('infogentsia_newsletters_gallery_template_id',$default_gallery_template_id),
        '#states' => array(
          'visible' => array(
            ':input[name="infogentsia_newsletters_template_type"]' => array('value' => 'gallery')
          )
        ),
        '#ajax' => array(
          'callback' => 'infogentsia_newsletters_template_callback',
          'method' => 'replace',
          'wrapper' => 'content-sections',
        ),
      );        
    }
    catch (Exception $e) {
      drupal_set_message(t('Unable to Load Templates!'), 'error');
      watchdog('infogentsia_newsletters', 'Unable to load templates for list @list. Error: "%message"', array(
        '@list' => $last_list_id,
        '%message' => $e->getMessage(),
      ), WATCHDOG_ERROR);
    }
  }

  $form['infogentsia_newsletters_content']['infogentsia_newsletters_content_templates'] = array(
    '#type' => 'fieldset',
    '#title' => t('Content'),
    '#collapsible' => TRUE
  );

  // Default header and footer
  $form['infogentsia_newsletters_content']['infogentsia_newsletters_content_templates']['infogentsia_newsletters_header'] = array(
    '#type' => 'textarea',
    '#title' => t('Content header'),
    '#description' => t('The header to be displayed at the top of the content. You can use *|LOGO|* as a placeholder for the logo and *|SITE_NAME|* as a placeholder for the site name. These are populated when the campaign is updated.'),
    '#default_value' => variable_get('infogentsia_newsletters_header',''),
    '#required' => FALSE,
  );

  $form['infogentsia_newsletters_content']['infogentsia_newsletters_content_templates']['infogentsia_newsletters_topic_templates'] = array(
    '#type' => 'fieldset',
    '#title' => t('Template to use for Topics'),
    '#collapsible' => TRUE
  );
  // Default topic header
  $form['infogentsia_newsletters_content']['infogentsia_newsletters_content_templates']['infogentsia_newsletters_topic_templates']['infogentsia_newsletters_topic_header_template'] = array(
    '#type' => 'textarea',
    '#title' => t('Template to use for each topic\'s header.'),
    '#description' => t('The MailChimp to be used as a header for each topic.'),
    '#default_value' => variable_get('infogentsia_newsletters_topic_header_template','News briefings from *|FEED:URL|*<br/><strong>*|FEED:TITLE|*</strong><br/>*|FEED:DESCRIPTION|*<br/>In the *|FEED:DATE|* edition:<br/>*|MC:TOC|*'),
    '#required' => FALSE,
  );
  // Topic specific templates
  $form['infogentsia_newsletters_content']['infogentsia_newsletters_content_templates']['infogentsia_newsletters_topic_templates']['infogentsia_newsletters_topic_content_template'] = array(
    '#type' => 'textarea',
    '#title' => t('Template to use for each topic.'),
    '#description' => t('The MailChimp content to be placed within *|FEEDITEMS|* merge tags.'),
    '#default_value' => variable_get('infogentsia_newsletters_topic_content_template','<span class="feed-item">*|FEEDITEM:TITLE|*<br/><br/>By *|FEEDITEM:AUTHOR|* on *|FEEDITEM:DATE|*<br/>*|FEEDITEM:CONTENT_FULL|*<br/>Read in browser »<br/>*|FEEDITEM:TWITTER|* *|FEEDITEM:LIKE|*</span>'),
    '#required' => FALSE,
  );
  $num_items = array(
    '0' => '0',
    '1' => '1',
    '2' => '2',
    '3' => '3',
    '4' => '4',
    '5' => '5',
    '6' => '6',
    '7' => '7',
    '8' => '8',
    '9' => '9',
    '10' => '10'
  );    
  //string schedule optional - one of "daily", "weekly", "monthly" - defaults to "daily"
  $form['infogentsia_newsletters_content']['infogentsia_newsletters_content_templates']['infogentsia_newsletters_topic_templates']['infogentsia_newsletters_topic_count'] = array(
    '#type' => 'select',
    '#title' => t('Number of items per topic'),
    '#options' => $num_items,
    '#default_value' => variable_get('infogentsia_newsletters_topic_count','3'),
    '#description' => t('Select the number of items to show per topic.'),
  );
  // Default topic footer
  $form['infogentsia_newsletters_content']['infogentsia_newsletters_content_templates']['infogentsia_newsletters_topic_templates']['infogentsia_newsletters_topic_footer_template'] = array(
    '#type' => 'textarea',
    '#title' => t('Template to use for each topic\'s footer.'),
    '#description' => t('The MailChimp to be used as a header for each topic.'),
    '#default_value' => variable_get('infogentsia_newsletters_topic_footer_template',''),
    '#required' => FALSE,
  );

  $form['infogentsia_newsletters_content']['infogentsia_newsletters_content_templates']['infogentsia_newsletters_footer'] = array(
    '#type' => 'textarea',
    '#title' => t('Content footer'),
    '#description' => t('The footer to be displayed at the bottom of the content.'),
    '#default_value' => variable_get('infogentsia_newsletters_footer','Copyright © *|CURRENT_YEAR|* *|LIST:COMPANY|*, All rights reserved.<br/>*|IFNOT:ARCHIVE_PAGE|* *|LIST:DESCRIPTION|*<br/><br/>Our mailing address is:<br/>*|HTML:LIST_ADDRESS_HTML|**|END:IF|*'),
    '#required' => FALSE,
  );

  // Content Placement
  $form['infogentsia_newsletters_content']['infogentsia_newsletters_content_placement'] = array(
    '#type' => 'fieldset',
    '#title' => t('Content Placement'),
    '#prefix' => '<div id="content-sections">',
    '#suffix' => '</div>',
    '#collapsible' => TRUE
  );

  $mc_template = NULL;
  $sections = array();
  if (variable_get('infogentsia_newsletters_template_type','gallery') != '') {
    switch (variable_get('infogentsia_newsletters_template_type','gallery')) {
      case "user" :
        $mc_template = mailchimp_campaign_get_template(variable_get('infogentsia_newsletters_template_id',$default_user_template_id),TRUE);
        break;
      case "gallery" :
        $mc_template = mailchimp_campaign_get_template(variable_get('infogentsia_newsletters_gallery_template_id',$default_gallery_template_id),TRUE);
        break;
    }
    if (strpos($mc_template['info']['source'], 'mc:repeatable')) {
      drupal_set_message(t('WARNING: This template has repeating sections, which are not supported. You may want to select a different template.'), 'warning');
    }
    foreach ($mc_template['info']['default_content'] as $section => $content) {
      $sections[$section] = drupal_ucfirst($section);
    }    
    // Default header and footer
    $form['infogentsia_newsletters_content']['infogentsia_newsletters_content_placement']['infogentsia_newsletters_header_loc'] = array(
      '#type' => 'select',
      '#title' => t('Header template section'),
      '#options' => $sections,
      '#default_value' => variable_get('infogentsia_newsletters_header_loc',''),
      '#description' => t('Select a template section to contain the header.'),
    );

    // Topic specific templates
    $form['infogentsia_newsletters_content']['infogentsia_newsletters_content_placement']['infogentsia_newsletters_content_loc'] = array(
      '#type' => 'select',
      '#title' => t('Content template section'),
      '#options' => $sections,
      '#default_value' => variable_get('infogentsia_newsletters_content_loc',''),
      '#description' => t('Select a template section to contain the content.'),
    );
    
    $form['infogentsia_newsletters_content']['infogentsia_newsletters_content_placement']['infogentsia_newsletters_footer_loc'] = array(
      '#type' => 'select',
      '#title' => t('Footer template section'),
      '#options' => $sections,
      '#default_value' => variable_get('infogentsia_newsletters_footer_loc',''),
      '#description' => t('Select a template section to contain the footer.'),
    );    
  } else {
    $form['infogentsia_newsletters_content']['infogentsia_newsletters_content_placement']['infogentsia_newsletters_notemplate'] = array(
      '#markup' => t('<p>No template selected yet.</p>')        
    );
  }

  //type_opts  
  $form['infogentsia_newsletters_rss_schedule'] = array(
    '#type' => 'fieldset',
    '#title' => t('Schedule options for campaign'),
    '#description' => t('Set the various scheduling options for the campaign')
  );
  //string schedule optional - one of "daily", "weekly", "monthly" - defaults to "daily"
  $form['infogentsia_newsletters_rss_schedule']['infogentsia_newsletters_schedule'] = array(
    '#type' => 'select',
    '#title' => t('Schedule'),
    '#options' => array('daily'=>t('Daily'),'weekly'=>t('Weekly'),'monthly'=>t('Monthly')),
    '#default_value' => variable_get('infogentsia_newsletters_schedule','daily'),
    '#description' => t('Select a schedule to use.'),
  );
  //string schedule_hour optional - an hour between 0 and 24 - default to 4 (4am local time) - applies to all schedule types
  $hours = array(
    '0' => '0',
    '1' => '1',
    '2' => '2',
    '3' => '3',
    '4' => '4',
    '5' => '5',
    '6' => '6',
    '7' => '7',
    '8' => '8',
    '9' => '9',
    '10' => '10',
    '11' => '11',
    '12' => '12',
    '13' => '13',
    '14' => '14',
    '15' => '15',
    '16' => '16',
    '17' => '17',
    '18' => '18',
    '19' => '19',
    '20' => '20',
    '21' => '21',
    '22' => '22',
    '23' => '23'
  );
  $form['infogentsia_newsletters_rss_schedule']['infogentsia_newsletters_schedule_hour'] = array(
    '#type' => 'select',
    '#title' => t('Hour of the day to send'),
    '#options' => $hours,
    '#default_value' => variable_get('infogentsia_newsletters_schedule_hour','4'),
    '#description' => t('Default to 4 (4am local time) - applies to all schedule types.'),
  );
  //string schedule_weekday optional - for "weekly" only, a number specifying the day of the week to send: 0 (Sunday) - 6  (Saturday) - defaults to 1 (Monday)
  $days = array(
    '0' => t('Sunday'),
    '1' => t('Monday'),
    '2' => t('Tuesday'),
    '3' => t('Wednesday'),
    '4' => t('Thursday'),
    '5' => t('Friday'),
    '6' => t('Saturday')
  );
  $form['infogentsia_newsletters_rss_schedule']['infogentsia_newsletters_schedule_weekday'] = array(
    '#type' => 'select',
    '#title' => t('Day to send'),
    '#options' => $days,
    '#default_value' => variable_get('infogentsia_newsletters_schedule_weekday','1'),
    '#description' => t('Select the day of the week to send the campaign on.'),
    '#states' => array(
      'visible' => array(
        ':input[name="infogentsia_newsletters_schedule"]' => array('value' => 'weekly')
      )
    )
  );
  //string schedule_monthday optional - for "monthly" only, a number specifying the day of the month to send (1 - 28) or "last"  for the last day of a given month. Defaults to the 1st day of the month
  $monthday = array(
    '1' => '1st',
    '2' => '2nd',
    '3' => '3rd',
    '4' => '4th',
    '5' => '5th',
    '6' => '6th',
    '7' => '7th',
    '8' => '8th',
    '9' => '9th',
    '10' => '10th',
    '11' => '11th',
    '12' => '12th',
    '13' => '13th',
    '14' => '14th',
    '15' => '15th',
    '16' => '16th',
    '17' => '17th',
    '18' => '18th',
    '19' => '19th',
    '20' => '20th',
    '21' => '21st',
    '22' => '22nd',
    '23' => '23rd',
    '24' => '24th',
    '25' => '25th',
    '26' => '26th',
    '27' => '27th',
    '28' => '28th',
    'last' => t('Last day of the month')
  );
  $form['infogentsia_newsletters_rss_schedule']['infogentsia_newsletters_schedule_monthday'] = array(
    '#type' => 'select',
    '#title' => t('Day of the month'),
    '#options' => $monthday,
    '#default_value' => variable_get('infogentsia_newsletters_schedule_monthday',''),
    '#description' => t('Select the day of the month to send the campaign on.'),
    '#states' => array(
      'visible' => array(
        ':input[name="infogentsia_newsletters_schedule"]' => array('value' => 'monthly')
      )
    )
  ); 
  //array days optional - used for "daily" schedules only, an array of the ISO-8601 weekday numbers to send on
  $form['infogentsia_newsletters_rss_schedule']['infogentsia_newsletters_days_group'] = array(
    '#type' => 'fieldset',
    '#title' => t('Day of the week'),
    '#states' => array(
      'visible' => array(
        ':input[name="infogentsia_newsletters_schedule"]' => array('value' => 'daily')
      )
    )
  );
  //array bool optional used for "daily" schedules only, an array of the ISO-8601 weekday numbers to send on
  $days = array(
    '1' => t('Monday'),
    '2' => t('Tuesday'),
    '3' => t('Wednesday'),
    '4' => t('Thursday'),
    '5' => t('Friday'),
    '6' => t('Saturday'),
    '7' => t('Sunday')
  );

  $form['infogentsia_newsletters_rss_schedule']['infogentsia_newsletters_days_group']['infogentsia_newsletters_days'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Days'),
    '#options' => $days,
    '#default_value' => variable_get('infogentsia_newsletters_days','') ,
    '#description' => t('Send on the above days.')
  ); 
    
  $form['#submit'][] = '_infogentsia_newsletter_form_submit';
  return system_settings_form($form);
}


/**
 * AJAX callback when changing template ID.
 */
function infogentsia_newsletters_template_callback($form, $form_state) {
  $sections = array();

  // Determine sections based on template
  $mc_template = NULL;
  if (!empty($form_state['values']['infogentsia_newsletters_template_type'])) {

    switch ($form_state['values']['infogentsia_newsletters_template_type']) {
      case "user" :
        $mc_template = mailchimp_campaign_get_template($form_state['values']['infogentsia_newsletters_template_id']);
        break;
      case "gallery" :
        $mc_template = mailchimp_campaign_get_template($form_state['values']['infogentsia_newsletters_gallery_template_id']);
        break;
    }            
    if (strpos($mc_template['info']['source'], 'mc:repeatable')) {
      drupal_set_message(t('WARNING: This template has repeating sections, which are not supported. You may want to select a different template.'), 'warning');
    }
    foreach ($mc_template['info']['default_content'] as $section => $content) {
      $sections[$section] = drupal_ucfirst($section);
    }
  }

  // Default header and footer
  $form['infogentsia_newsletters_content_placement']['infogentsia_newsletters_header_loc'] = array(
    '#type' => 'select',
    '#title' => t('Header template section'),
    '#options' => $sections,
    '#default_value' => variable_get('infogentsia_newsletters_header_loc',''),
    '#description' => t('Select a template section to contain the header.'),
  );

  // Topic specific templates
  $form['infogentsia_newsletters_content_placement']['infogentsia_newsletters_content_loc'] = array(
    '#type' => 'select',
    '#title' => t('Content template section'),
    '#options' => $sections,
    '#default_value' => variable_get('infogentsia_newsletters_content_loc',''),
    '#description' => t('Select a template section to contain the content.'),
  );

  $form['infogentsia_newsletters_content_placement']['infogentsia_newsletters_footer_loc'] = array(
    '#type' => 'select',
    '#title' => t('Footer template section'),
    '#options' => $sections,
    '#default_value' => variable_get('infogentsia_newsletters_footer_loc',''),
    '#description' => t('Select a template section to contain the footer.'),
  );

  return $form['infogentsia_newsletters_content_placement'];
}

function infogentsia_newsletter_populate_interest_groupings($form,$form_state) {
  $mcapi = mailchimp_get_api_object();

  // Get interest groups
  $interestGroupings = $mcapi->lists->interestGroupings($form_state['values']['infogentsia_newsletters_list']);

  $options = array();
  if (is_array($interestGroupings)) {
    foreach($interestGroupings as $interestGrouping) {
      $options[$interestGrouping['id']] = $interestGrouping['name'];
    }
  }
  $form['infogentsia_newsletters_interest_list_group'] = array(
    '#type' => 'select',
    '#title' => t('Interest Grouping'),
    '#options' => $options,
    '#default_value' => variable_get('infogentsia_newsletters_interest_list_group',''),
    '#prefix' => '<div class="squadron-infogentsia_newsletters_interest_list_group" id="nil-group">', 
    '#suffix' => '</div>',
    '#description' => t('Select an existing interest grouping from MailChimp. This grouping needs to be created within MailChimp and have a group of "All Topics" created manually. All other groups will be maintained through this interface.')
  );  
  return $form['infogentsia_newsletters_interest_list_group'];
}

// Submit the form
function _infogentsia_newsletter_form_submit($form, &$form_state) {
  // Retreive the current settings for the campaign
  $campaign_settings = $form_state['values'];
  //foreach($campaign_settings as $setting=>$val) drupal_set_message('Set ' . $setting . ' to ' . $val);
  _infogentsia_newsletters_create_campaign($campaign_settings); 
}

