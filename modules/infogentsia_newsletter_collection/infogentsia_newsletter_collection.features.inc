<?php
/**
 * @file
 * infogentsia_newsletter_collection.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function infogentsia_newsletter_collection_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_default_mailchimp_signup().
 */
function infogentsia_newsletter_collection_default_mailchimp_signup() {
  $items = array();
  $items['newsletters_signup'] = entity_import('mailchimp_signup', '{
    "name" : "newsletters_signup",
    "mode" : "2",
    "title" : "News Briefing Signup",
    "settings" : {
      "path" : "news-briefings",
      "submit_button" : "Subscribe",
      "confirmation_message" : "You have been successfully subscribed.",
      "destination" : "",
      "mergefields" : {
        "EMAIL" : {
          "name" : "Email Address",
          "req" : true,
          "field_type" : "email",
          "public" : true,
          "show" : true,
          "order" : "1",
          "default" : null,
          "helptext" : null,
          "size" : "25",
          "tag" : "EMAIL",
          "id" : 0
        },
        "FNAME" : {
          "name" : "First Name",
          "req" : false,
          "field_type" : "text",
          "public" : true,
          "show" : true,
          "order" : "2",
          "default" : "",
          "helptext" : "",
          "size" : "25",
          "tag" : "FNAME",
          "id" : 1
        },
        "LNAME" : {
          "name" : "Last Name",
          "req" : false,
          "field_type" : "text",
          "public" : true,
          "show" : true,
          "order" : "3",
          "default" : "",
          "helptext" : "",
          "size" : "25",
          "tag" : "LNAME",
          "id" : 2
        }
      },
      "description" : "",
      "doublein" : 0,
      "send_welcome" : 1,
      "include_interest_groups" : 1
    }
  }');
  return $items;
}
