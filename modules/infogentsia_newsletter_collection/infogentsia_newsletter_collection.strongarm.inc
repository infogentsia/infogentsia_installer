<?php
/**
 * @file
 * infogentsia_newsletter_collection.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function infogentsia_newsletter_collection_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'infogentsia_newsletters_vocabulary';
  $strongarm->value = '1';
  $export['infogentsia_newsletters_vocabulary'] = $strongarm;

  return $export;
}
