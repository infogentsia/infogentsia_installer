<?php
/**
 * @file
 * infogentsia_feeds.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function infogentsia_feeds_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'topic_importer';
  $feeds_importer->config = array(
    'name' => 'Stream Article Importer',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'FeedsInfogentsiaFetcher',
      'config' => array(
        'auto_detect_feeds' => 1,
        'use_pubsubhubbub' => 0,
        'designated_hub' => '',
        'request_timeout' => '',
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsSyndicationParser',
      'config' => array(),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'bundle' => 'article',
        'update_existing' => '2',
        'expire' => '-1',
        'mappings' => array(
          0 => array(
            'source' => 'title',
            'target' => 'title',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'timestamp',
            'target' => 'created',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'guid',
            'target' => 'guid',
            'unique' => 1,
          ),
          3 => array(
            'source' => 'description',
            'target' => 'body',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'url',
            'target' => 'field_article_url',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'author_name',
            'target' => 'field_article_author',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'parent:nid',
            'target' => 'field_stream:etid',
            'unique' => FALSE,
          ),
          7 => array(
            'source' => 'parent:taxonomy:topics',
            'target' => 'field_topic',
            'term_search' => '1',
            'autocreate' => 0,
          ),
        ),
        'input_format' => 'feed_format',
        'author' => '2',
        'authorize' => 0,
        'skip_hash_check' => 0,
      ),
    ),
    'content_type' => 'stream',
    'update' => 0,
    'import_period' => '1800',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 0,
  );
  $export['topic_importer'] = $feeds_importer;

  return $export;
}
