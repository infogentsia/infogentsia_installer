<?php
/**
 * @file
 * infogentsia_views.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function infogentsia_views_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'views_defaults';
  $strongarm->value = array(
    'frontpage' => TRUE,
    'archive' => TRUE,
    'category' => TRUE,
    'feeds_log' => TRUE,
    'portfolio' => TRUE,
    'group_home_page_content' => TRUE,
    'services' => TRUE,
    'tags' => TRUE,
    'rss' => TRUE,
    'categories_portfolio' => TRUE,
    'topic_pages' => TRUE,
    'filtered_vertical_list_page_' => TRUE,
    'topic_feed' => TRUE,
    'taxonomy_term_list_all' => TRUE,
    'slide_show' => TRUE,
    'latest_posts' => TRUE,
    'og_menu' => TRUE,
    'blogs' => TRUE,
    'topics_list' => TRUE,
    'nodes_autocomplete' => TRUE,
  );
  $export['views_defaults'] = $strongarm;

  return $export;
}
