<?php
/**
 * @file
 * infogentsia_admin.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function infogentsia_admin_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-quick-links_administer-infogentsia-newsletters:admin/config/services/infogentsia_newsletters
  $menu_links['menu-quick-links_administer-infogentsia-newsletters:admin/config/services/infogentsia_newsletters'] = array(
    'menu_name' => 'menu-quick-links',
    'link_path' => 'admin/config/services/infogentsia_newsletters',
    'router_path' => 'admin/config/services',
    'link_title' => 'Administer Infogentsia Newsletters',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-quick-links_administer-infogentsia-newsletters:admin/config/services/infogentsia_newsletters',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 1,
    'weight' => -50,
    'customized' => 1,
    'parent_identifier' => 'menu-quick-links_administer-infogentsia:admin/config/services/infogentsia',
  );
  // Exported menu link: menu-quick-links_administer-infogentsia:admin/config/services/infogentsia
  $menu_links['menu-quick-links_administer-infogentsia:admin/config/services/infogentsia'] = array(
    'menu_name' => 'menu-quick-links',
    'link_path' => 'admin/config/services/infogentsia',
    'router_path' => 'admin/config/services/infogentsia',
    'link_title' => 'Administer Infogentsia',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-quick-links_administer-infogentsia:admin/config/services/infogentsia',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 1,
    'weight' => -49,
    'customized' => 1,
  );
  // Exported menu link: menu-quick-links_administer-mailchimp:admin/config/services/mailchimp
  $menu_links['menu-quick-links_administer-mailchimp:admin/config/services/mailchimp'] = array(
    'menu_name' => 'menu-quick-links',
    'link_path' => 'admin/config/services/mailchimp',
    'router_path' => 'admin/config/services',
    'link_title' => 'Administer MailChimp',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-quick-links_administer-mailchimp:admin/config/services/mailchimp',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'parent_identifier' => 'menu-quick-links_administer-infogentsia:admin/config/services/infogentsia',
  );
  // Exported menu link: menu-quick-links_administer-users:admin/people
  $menu_links['menu-quick-links_administer-users:admin/people'] = array(
    'menu_name' => 'menu-quick-links',
    'link_path' => 'admin/people',
    'router_path' => 'admin/people',
    'link_title' => 'Administer Users',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-quick-links_administer-users:admin/people',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 1,
    'weight' => -50,
    'customized' => 1,
  );
  // Exported menu link: menu-quick-links_administer-infogentsia:admin/config/user-interface/info-branding
  $menu_links['menu-quick-links_administer-infogentsia:admin/config/user-interface/info-branding'] = array(
    'menu_name' => 'menu-quick-links',
    'link_path' => 'admin/config/user-interface/info-branding',
    'router_path' => 'admin/config/user-interface',
    'link_title' => 'Administer Branding',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-quick-links_administer-infogentsia:admin/config/user-interface/info-branding',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 1,
    'weight' => -50,
    'customized' => 1,
    'parent_identifier' => 'menu-quick-links_administer-infogentsia:admin/config/services/infogentsia',
  );
  // Exported menu link: menu-quick-links_administer-infogentsia:frontpage/sort
  $menu_links['menu-quick-links_administer-infogentsia:frontpage/sort'] = array(
    'menu_name' => 'menu-quick-links',
    'link_path' => 'frontpage/sort',
    'router_path' => 'frontpage',
    'link_title' => 'Change Order of Topics',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-quick-links_administer-infogentsia:frontpage/sort',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 1,
    'weight' => -50,
    'customized' => 1,
    'parent_identifier' => 'menu-quick-links_administer-infogentsia:admin/config/services/infogentsia',
  );
  // Exported menu link: menu-quick-links_administer-users:admin/people/create
  $menu_links['menu-quick-links_administer-users:admin/people/create'] = array(
    'menu_name' => 'menu-quick-links',
    'link_path' => 'admin/people/create',
    'router_path' => 'admin/people/create',
    'link_title' => 'Creater User',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-quick-links_administer-users:admin/people/create',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 1,
    'weight' => -50,
    'customized' => 1,
    'parent_identifier' => 'menu-quick-links_administer-users:admin/people',
  );
  // Exported menu link: menu-quick-links_create-a-stream:node/add/stream
  $menu_links['menu-quick-links_create-a-stream:node/add/stream'] = array(
    'menu_name' => 'menu-quick-links',
    'link_path' => 'node/add/stream',
    'router_path' => 'node/add',
    'link_title' => 'Create a Stream',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-quick-links_create-a-stream:node/add/stream',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'parent_identifier' => 'menu-quick-links_find-content:admin/content',
  );
  // Exported menu link: menu-quick-links_find-content:admin/content
  $menu_links['menu-quick-links_find-content:admin/content'] = array(
    'menu_name' => 'menu-quick-links',
    'link_path' => 'admin/content',
    'router_path' => 'admin/content',
    'link_title' => 'Find Content',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-quick-links_find-content:admin/content',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 1,
    'weight' => -48,
    'customized' => 1,
  );
  // Exported menu link: menu-quick-links_manage-topics:admin/structure/taxonomy/topics/add
  $menu_links['menu-quick-links_manage-topics:admin/structure/taxonomy/topics/add'] = array(
    'menu_name' => 'menu-quick-links',
    'link_path' => 'admin/structure/taxonomy/topics/add',
    'router_path' => 'admin/structure/taxonomy/%/add',
    'link_title' => 'Create a Topic',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-quick-links_manage-topics:admin/structure/taxonomy/topics/add',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
    'parent_identifier' => 'menu-quick-links_find-content:admin/content',
  );
  // Exported menu link: menu-quick-links_manage-topics:admin/structure/taxonomy/topics
  $menu_links['menu-quick-links_manage-topics:admin/structure/taxonomy/topics'] = array(
    'menu_name' => 'menu-quick-links',
    'link_path' => 'admin/structure/taxonomy/topics',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Manage Topics',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-quick-links_manage-topics:admin/structure/taxonomy/topics',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -46,
    'customized' => 1,
    'parent_identifier' => 'menu-quick-links_find-content:admin/content',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Administer Branding');
  t('Administer Infogentsia');
  t('Administer Infogentsia Newsletters');
  t('Administer MailChimp');
  t('Administer Users');
  t('Create a Stream');
  t('Find Content');
  t('Create a Topic');
  t('Manage Topics');


  return $menu_links;
}
