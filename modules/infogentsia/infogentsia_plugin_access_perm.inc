<?php

/**
 * @file
 * Definition of views_plugin_access_perm.
 */

/**
 * Access plugin that provides permission-based access control.
 *
 * @ingroup views_access_plugins
 */
class infogentsia_plugin_access_perm extends views_plugin_access {

  function access($account) {
    return infogentsia_rss_ua_access($this->options['user-agent'], $account);
  }

  function get_access_callback() {
    return array('infogentsia_rss_ua_access', array($this->options['user-agent']));
  }

  function summary_title() {
    return t('User-Agent: ' . $this->options['user-agent']);
  }


  function option_definition() {
    $options = parent::option_definition();
    $options['user-agent'] = array('default' => '');

    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['user-agent'] = array(
      '#type' => 'textfield',
      '#title' => t('User-Agent'),
      '#default_value' => $this->options['user-agent'],
      '#description' => t('Allow access to this view only if the request header includes this user agent string. This is an exact, case sensetive match. User with the \'Bypass RSS User-Agent block\' permission are able to bypass this.'),
    );
  }
}
