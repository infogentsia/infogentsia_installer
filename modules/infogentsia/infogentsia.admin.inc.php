<?php

function infogentsia_settings() {

  $noptions = array();  

  $types = node_type_get_types();
  foreach($types as $node_type) {
    $noptions[$node_type->type] = t($node_type->name);
  }
  
  $form['infogentsia_premium_content'] = array();

  // Premium content type
  $form['infogentsia_premium_content']['infogentsia_premium_type'] = array(
    '#type' => 'select',
    '#title' => t('Content type protected by premium content flag'),
    '#options' => $noptions,
    '#default_value' => variable_get('infogentsia_premium_type', 'feed'),
    '#description' => t('What content type is being used as possible Premium Content?')
  );

  return system_settings_form($form);
}