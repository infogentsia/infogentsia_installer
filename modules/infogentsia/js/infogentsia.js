jQuery(document).ready(function () {
    // Registration form
    jQuery('#edit-profile-referrer').change(function () {
        if (jQuery('#edit-profile-referrer').val() == 'Other') {
            jQuery('.form-item-profile-referrer-other').show();
        } else {
            jQuery('.form-item-profile-referrer-other').hide();
        }
    });


    // Correct List View URLs
    // Determine if we're looking at list view
    var uri = document.location.pathname.split("/");
    var view = uri[uri.length - 1];
    jQuery('#block-menu-block-1 .textwidget li a').each(function () {
        newHref = jQuery(this).attr("href");
        jQuery(this).attr("href", newHref + '/' + view);
    });
    jQuery('#block-menu-block-1 .textwidget li').delegate('a', 'click', function (event) {
        event.preventDefault();
        // get target link
        var target = jQuery(this).attr('href');
        // Update URL
        history.pushState(null, jQuery(this).text(), target);
        // Load the data
        // Add parameter to the end of the url to load embed data
        append = (target.indexOf("?") > -1) ? "&" : "?";
        loadUrl = target + append + 'response_type=embed';
        jQuery.ajax({
            type: "GET",
            url: loadUrl,
            dataType: "html",
            context: { pagetitle: jQuery(this).text() },
            success: function (data) {
                jQuery('#block-system-main').html(jQuery(data).find('#block-system-main'));
                jQuery('.page-title').text(this.pagetitle);
                dynamicPager();
            },
            error: function () {
                alert("Error");
            }
        })
    });
    dynamicPager();
});
function dynamicPager() {
    jQuery('#block-system-main ul.pager li').delegate('a', 'click', function (event) {
        event.preventDefault();
        // get target link
        var target = jQuery(this).attr('href').replace('&response_type=embed', '').replace('?response_type=embed', '');
        // Update URL
        history.pushState(null, jQuery(this).text(), target);
        // Load the data
        // Add parameter to the end of the url to load embed data
        append = (target.indexOf("?") > -1) ? "&" : "?";
        loadUrl = target + append + 'response_type=embed';
        jQuery.ajax({
            type: "GET",
            url: loadUrl,
            dataType: "html",
            success: function (data) {
                jQuery('#block-system-main').html(jQuery(data).find('#block-system-main'));
                // Strip the embed portion from the target
                jQuery('#block-system-main ul.pager li a').each(function(index) {
                  jQuery(this).attr('href', jQuery(this).attr('href').replace('&response_type=embed', '').replace('?response_type=embed', ''));  
                })
                dynamicPager();
            },
            error: function () {
                alert("Error");
            }
        })
    });
}