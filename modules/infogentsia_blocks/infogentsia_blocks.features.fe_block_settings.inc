<?php
/**
 * @file
 * infogentsia_blocks.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function infogentsia_blocks_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['infogentsia-infogentsia_copyright'] = array(
    'cache' => 1,
    'custom' => 0,
    'delta' => 'infogentsia_copyright',
    'module' => 'infogentsia',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 4,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 4,
      ),
      'sq' => array(
        'region' => 'footer_copyright',
        'status' => 1,
        'theme' => 'sq',
        'weight' => 4,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['infogentsia-infogentsia_power_by'] = array(
    'cache' => 1,
    'custom' => 0,
    'delta' => 'infogentsia_power_by',
    'module' => 'infogentsia',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 1,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 1,
      ),
      'sq' => array(
        'region' => 'footer_copyright',
        'status' => 1,
        'theme' => 'sq',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['infogentsia-infogentsia_site_menu'] = array(
    'cache' => 1,
    'custom' => 0,
    'delta' => 'infogentsia_site_menu',
    'module' => 'infogentsia',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'sq' => array(
        'region' => 'sidebar_top_menu',
        'status' => 1,
        'theme' => 'sq',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['infogentsia_newsletters-infogentsia_newsletters_sub'] = array(
    'cache' => 1,
    'custom' => 0,
    'delta' => 'infogentsia_newsletters_sub',
    'module' => 'infogentsia_newsletters',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'sq' => array(
        'region' => 'subscribe',
        'status' => 1,
        'theme' => 'sq',
        'weight' => -15,
      ),
    ),
    'title' => '<none>',
    'visibility' => 0,
  );

  $export['menu-menu-quick-links'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'menu-quick-links',
    'module' => 'menu',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => 'dashboard_sidebar',
        'status' => 1,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'sq' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sq',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['infogentsia-dashboard_instructions'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'dashboard_instructions',
    'module' => 'infogentsia',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => 'dashboard_main',
        'status' => 1,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'sq' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sq',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['menu-menu-secondary-links'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'menu-secondary-links',
    'module' => 'menu',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'sq' => array(
        'region' => 'footer_copyright',
        'status' => 1,
        'theme' => 'sq',
        'weight' => 3,
      ),
    ),
    'title' => 'using Infogentsia',
    'visibility' => 0,
  );

  $export['search-form'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'form',
    'module' => 'search',
    'node_types' => array(),
    'pages' => 's/*
search
search/*',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => 'sidebar_first',
        'status' => 1,
        'theme' => 'bartik',
        'weight' => -1,
      ),
      'seven' => array(
        'region' => 'dashboard_inactive',
        'status' => 1,
        'theme' => 'seven',
        'weight' => -10,
      ),
      'sq' => array(
        'region' => 'search',
        'status' => 1,
        'theme' => 'sq',
        'weight' => -15,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['system-main'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'main',
    'module' => 'system',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'sq' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'sq',
        'weight' => -14,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['system-user-menu'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'user-menu',
    'module' => 'system',
    'node_types' => array(),
    'pages' => 'user
user/*',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'sq' => array(
        'region' => 'sidebar',
        'status' => 1,
        'theme' => 'sq',
        'weight' => -15,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['infogentsia-infogentsia_phone'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'infogentsia_phone',
    'module' => 'infogentsia',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'sq' => array(
        'region' => 'footer_copyright',
        'status' => 1,
        'theme' => 'sq',
        'weight' => 2,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['infogentsia_newsletters-infogentsia_newsletters_cc'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'infogentsia_newsletters_cc',
    'module' => 'infogentsia_newsletters',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => 'dashboard_main',
        'status' => 1,
        'theme' => 'seven',
        'weight' => 4,
      ),
      'sq' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sq',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['menu-devel'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'devel',
    'module' => 'menu',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => 'dashboard_sidebar',
        'status' => 1,
        'theme' => 'seven',
        'weight' => 5,
      ),
      'sq' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sq',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['user-login'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'login',
    'module' => 'user',
    'node_types' => array(),
    'pages' => 403,
    'roles' => array(),
    'themes' => array(
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'sq' => array(
        'region' => 'sidebar',
        'status' => 1,
        'theme' => 'sq',
        'weight' => 1,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  return $export;
}
