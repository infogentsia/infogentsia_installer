<?php
/**
 * @file
 * infogentsia_core.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function infogentsia_core_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'features_ignored_orphans';
  $strongarm->value = array();
  $export['features_ignored_orphans'] = $strongarm;

  return $export;
}
