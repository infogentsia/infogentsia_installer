<?php
/**
 * @file
 * infogentsia_ui.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function infogentsia_ui_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_default_active_menus';
  $strongarm->value = array(
    1 => 'features',
    3 => 'main-menu',
    4 => 'management',
    5 => 'navigation',
    6 => 'menu-secondary-links',
    7 => 'user-menu',
    8 => 'menu-quick-links',
    9 => 'devel',
  );
  $export['menu_default_active_menus'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_expanded';
  $strongarm->value = array(
    0 => 'menu-quick-links',
  );
  $export['menu_expanded'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'theme_default';
  $strongarm->value = 'sq';
  $export['theme_default'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'theme_settings';
  $strongarm->value = array(
    'toggle_logo' => 1,
    'toggle_name' => 1,
    'toggle_slogan' => 1,
    'toggle_node_user_picture' => 1,
    'toggle_comment_user_picture' => 1,
    'toggle_comment_user_verification' => 1,
    'toggle_favicon' => 1,
    'toggle_main_menu' => 1,
    'toggle_secondary_menu' => 1,
    'default_logo' => 0,
    'logo_path' => 'profiles/infogentsia_installer/themes/sq/img/logo.jpg',
    'logo_upload' => '',
    'default_favicon' => 0,
    'favicon_path' => 'profiles/infogentsia_installer/themes/sq/img/favicon.ico',
    'favicon_upload' => '',
  );
  $export['theme_settings'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'theme_sq_settings';
  $strongarm->value = array(
    'toggle_logo' => 1,
    'toggle_name' => 1,
    'toggle_slogan' => 1,
    'toggle_node_user_picture' => 1,
    'toggle_comment_user_picture' => 1,
    'toggle_comment_user_verification' => 1,
    'toggle_favicon' => 1,
    'toggle_main_menu' => 1,
    'toggle_secondary_menu' => 1,
    'default_logo' => 0,
    'logo_path' => 'profiles/infogentsia_installer/themes/sq/img/logo.jpg',
    'logo_upload' => '',
    'default_favicon' => 0,
    'favicon_path' => 'profiles/infogentsia_installer/themes/sq/img/favicon.ico',
    'favicon_upload' => '',
    'tm_value_13' => 'Arial',
    'tm_value_4' => 0,
    'tm_value_12' => 'Arial:300,400,700',
    'tm_value_5' => 0,
    'tm_value_14' => 'arial',
    'tm_value_15' => 0,
    'tm_value_6' => '',
    'tm_value_0' => 'infogentsia',
    'tm_value_16' => 'company/3561243',
    'tm_value_2' => 'content/contact-us',
    'tm_value_7' => 'fade',
    'tm_value_11' => 'true',
    'tm_value_9' => '2000',
    'tm_value_10' => '600',
  );
  $export['theme_sq_settings'] = $strongarm;

  return $export;
}
