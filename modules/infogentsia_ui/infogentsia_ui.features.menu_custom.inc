<?php
/**
 * @file
 * infogentsia_ui.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function infogentsia_ui_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-secondary-links.
  $menus['menu-secondary-links'] = array(
    'menu_name' => 'menu-secondary-links',
    'title' => 'Secondary Links',
    'description' => 'Links to display in the lower right block of the site',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Links to display in the lower right block of the site');
  t('Secondary Links');


  return $menus;
}
