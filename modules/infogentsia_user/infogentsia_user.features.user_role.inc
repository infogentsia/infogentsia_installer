<?php
/**
 * @file
 * infogentsia_user.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function infogentsia_user_user_default_roles() {
  $roles = array();

  // Exported role: Content Manager.
  $roles['Content Manager'] = array(
    'name' => 'Content Manager',
    'weight' => 5,
  );

  // Exported role: Feed Admin.
  $roles['Feed Admin'] = array(
    'name' => 'Feed Admin',
    'weight' => 4,
  );

  // Exported role: Premium User.
  $roles['Premium User'] = array(
    'name' => 'Premium User',
    'weight' => 2,
  );

  // Exported role: Site Manager.
  $roles['Site Manager'] = array(
    'name' => 'Site Manager',
    'weight' => 6,
  );

  // Exported role: administrator.
  $roles['administrator'] = array(
    'name' => 'administrator',
    'weight' => 7,
  );

  return $roles;
}
