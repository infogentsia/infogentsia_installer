<?php
/**
 * @file
 * infogentsia_user.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function infogentsia_user_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_fe_profile_export_fields().
 */
function infogentsia_user_fe_profile_export_fields() {
  $items = array(
    'profile_city' => array(
      'fid' => 5,
      'title' => 'City',
      'name' => 'profile_city',
      'explanation' => '',
      'category' => 'Contact Information',
      'page' => '',
      'type' => 'textfield',
      'weight' => 0,
      'required' => 0,
      'register' => 1,
      'visibility' => 2,
      'autocomplete' => 0,
      'options' => '',
    ),
    'profile_company' => array(
      'fid' => 3,
      'title' => 'Company',
      'name' => 'profile_company',
      'explanation' => '',
      'category' => 'Contact Information',
      'page' => '',
      'type' => 'textfield',
      'weight' => 0,
      'required' => 0,
      'register' => 1,
      'visibility' => 2,
      'autocomplete' => 0,
      'options' => '',
    ),
    'profile_first_name' => array(
      'fid' => 1,
      'title' => 'First Name',
      'name' => 'profile_first_name',
      'explanation' => '',
      'category' => 'Contact Information',
      'page' => '',
      'type' => 'textfield',
      'weight' => 0,
      'required' => 1,
      'register' => 1,
      'visibility' => 2,
      'autocomplete' => 0,
      'options' => '',
    ),
    'profile_last_name' => array(
      'fid' => 2,
      'title' => 'Last Name',
      'name' => 'profile_last_name',
      'explanation' => '',
      'category' => 'Contact Information',
      'page' => '',
      'type' => 'textfield',
      'weight' => 0,
      'required' => 1,
      'register' => 1,
      'visibility' => 2,
      'autocomplete' => 0,
      'options' => '',
    ),
    'profile_referrer' => array(
      'fid' => 7,
      'title' => 'How did you hear about us?',
      'name' => 'profile_referrer',
      'explanation' => '',
      'category' => 'Contact Information',
      'page' => '',
      'type' => 'selection',
      'weight' => 0,
      'required' => 1,
      'register' => 1,
      'visibility' => 2,
      'autocomplete' => 0,
      'options' => 'Web Search
Twitter
LinkedIn
Other Social Media
Personal Referral
News Article
Blog Post
Other',
    ),
    'profile_referrer_other' => array(
      'fid' => 8,
      'title' => 'Other',
      'name' => 'profile_referrer_other',
      'explanation' => '',
      'category' => 'Contact Information',
      'page' => '',
      'type' => 'textfield',
      'weight' => 0,
      'required' => 0,
      'register' => 1,
      'visibility' => 2,
      'autocomplete' => 0,
      'options' => '',
    ),
    'profile_state' => array(
      'fid' => 6,
      'title' => 'State/Province',
      'name' => 'profile_state',
      'explanation' => '',
      'category' => 'Contact Information',
      'page' => '',
      'type' => 'selection',
      'weight' => 0,
      'required' => 0,
      'register' => 1,
      'visibility' => 2,
      'autocomplete' => 0,
      'options' => 'Alabama
Alaska
Alberta
American Samoa
Arizona
Arkansas
British Columbia
California
Colorado
Connecticut
Delaware
District of Columbia
Fed. States of Micronesia
Florida
Georgia
Guam
Hawaii
Idaho
Illinois
Indiana
Iowa
Kansas
Kentucky
Louisiana
Maine
Manitoba
Marshall Islands
Maryland
Massachusetts
Michigan
Minnesota
Mississippi
Missouri
Montana
Nebraska
Nevada
New Hampshire
New Jersey
New Mexico
New York
New Brunswick
Newfoundland
North Carolina
North Dakota
Northern Mariana Is.
Northwest Territories
Nova Scotia
Ohio
Oklahoma
Ontario
Oregon
Palau
Pennsylvania
Prince Edward Island
Puerto Rico
Quebec
Rhode Island
Saskatchewan
South Carolina
South Dakota
Tennessee
Texas
Utah
Vermont
Virginia
Virgin Islands
Washington
West Virginia
Wisconsin
Wyoming
Yukon',
    ),
    'profile_title' => array(
      'fid' => 4,
      'title' => 'Title',
      'name' => 'profile_title',
      'explanation' => '',
      'category' => 'Contact Information',
      'page' => '',
      'type' => 'textfield',
      'weight' => 0,
      'required' => 0,
      'register' => 1,
      'visibility' => 2,
      'autocomplete' => 0,
      'options' => '',
    ),
  );
  return $items;
}
