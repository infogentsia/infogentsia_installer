<?php
/**
 * @file
 * infogentsia_rating.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function infogentsia_rating_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'view rate results page'.
  $permissions['view rate results page'] = array(
    'name' => 'view rate results page',
    'roles' => array(
      'Content Manager' => 'Content Manager',
    ),
    'module' => 'rate',
  );

  return $permissions;
}
