<?php
/**
 * @file
 * infogentsia_rating.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function infogentsia_rating_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
