<?php
/**
 * @file
 * infogentsia_rating.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function infogentsia_rating_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'rate_widgets';
  $strongarm->value = array(
    1 => (object) array(
      'name' => 'article_relevance',
      'tag' => 'vote',
      'title' => 'Article Relevance',
      'node_types' => array(
        0 => 'article',
      ),
      'comment_types' => array(),
      'options' => array(
        0 => array(
          0 => 1,
          1 => 'up',
        ),
        1 => array(
          0 => -1,
          1 => 'down',
        ),
      ),
      'template' => 'infogentsia_thumbs_up_down',
      'node_display' => '2',
      'teaser_display' => FALSE,
      'comment_display' => '0',
      'node_display_mode' => '2',
      'teaser_display_mode' => '2',
      'comment_display_mode' => '1',
      'roles' => array(
        2 => '2',
        7 => 0,
        1 => 0,
        3 => 0,
        4 => 0,
        5 => 0,
        6 => 0,
      ),
      'allow_voting_by_author' => 1,
      'noperm_behaviour' => '3',
      'displayed' => '1',
      'displayed_just_voted' => '2',
      'description' => '',
      'description_in_compact' => TRUE,
      'delete_vote_on_second_click' => '0',
      'use_source_translation' => TRUE,
      'value_type' => 'points',
      'theme' => 'rate_template_infogentsia_thumbs_up_down',
      'css' => 'profiles/infogentsia_installer/modules/infogentsia_newsletters/rate_templates/infogentsia-thumbs-up-down/infogentsia-thumbs-up-down.css',
      'translate' => TRUE,
    ),
  );
  $export['rate_widgets'] = $strongarm;

  return $export;
}
