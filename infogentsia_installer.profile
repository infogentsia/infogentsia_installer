<?php
/**
 * @file
 * Enables modules and site configuration for a standard site installation.
 */

$inc_path = drupal_get_path('profile','infogentsia_installer');
$includes = array('utilities','configure_mailchimp','configure_mailchimp_list','configure_mailchimp_group','configure_mollom','configure_ga','default_content','configure_social','finish_infogentsia');
foreach ($includes as $inc) {
  include_once($inc_path . '/includes/' . $inc . '.php');
}

function infogentsia_installer_install_tasks_alter(&$tasks, $install_state) {
  // Hide the database configuration step.
  $tasks['install_settings_form'] = array(
    'display_name' => st('Database setup'),
    'display' => TRUE,
    'function' => '_infogentsia_installer_install_settings_override'
  );
  $tasks['install_finished'] = array(
    'display_name' => st('Finished'),
    'display' => TRUE,
    'function' => '_infogentsia_install_finished_override'
  );
  $tasks['install_profile_modules'] = array(
    'display_name' => st('Install site'),
    'display' => TRUE,
    'type' => 'batch',
    'function' => '_infogentsia_installer_install_profile_modules_override'
  );
}

function infogentsia_installer_install_tasks(&$install_state) {
  $task['_infogentsia_installer_mailchimp'] = array(
    'display_name' => st('Configure MailChimp'),
    'display' => TRUE,
    'type' => 'form',
  );
  $task['_infogentsia_installer_mailchimp_list'] = array(
    'display_name' => st('Configure MailChimp List'),
    'display' => TRUE,
    'type' => 'form',
  );
  $task['_infogentsia_installer_mailchimp_group'] = array(
    'display_name' => st('Configure MailChimp Group'),
    'display' => TRUE,
    'type' => 'form',
  );  
  $task['_infogentsia_installer_mollom'] = array(
    'display_name' => st('Configure Mollom'),
    'display' => TRUE,
    'type' => 'form',
  );
  $task['_infogentsia_installer_googleanalytics'] = array(
    'display_name' => st('Configure Google Analytics'),
    'display' => TRUE,
    'type' => 'form',
  );
  $task['_infogentsia_installer_social'] = array(
    'display_name' => st('Configure Social Accounts'),
    'display' => TRUE,
    'type' => 'form',
  );
  $task['_infogentsia_installer_default_content'] = array(
    'display_name' => st('Create default content'),
    'display' => TRUE,
  );
  $task['_infogentsia_installer_configure'] = array(
    'display_name' => st('Final Infogentsia configuration'),
    'display' => TRUE,
  );
  return $task;
}

function _infogentsia_installer_install_settings_override() {  
  $db_creds = _infogentsia_installer_db_creds();
  global $install_state;
  $database['default']['default'] = array(
    'driver'   => 'mysql',
    'database' => $db_creds[0],
    'username' => $db_creds[0],
    'password' => $db_creds[1],
    'host'     => 'localhost',
    'port'     => '',
    'prefix'   => '',
  );
  $settings['databases'] = array(
    'value' =>$database,
    'required' => TRUE,
  );
  $settings['drupal_hash_salt'] = array(
    'value' => drupal_hash_base64(drupal_random_bytes(55)),
    'required' => TRUE,
  );
  drupal_rewrite_settings($settings);
  $install_state['settings_verified'] = TRUE;
  $install_state['completed_task'] = install_verify_completed_task();
}

function _infogentsia_installer_install_profile_modules_override(&$install_state) {
  // Set the customizations to the installer theme
  _infogentsia_installer_set_logo();
  // Process the installation
  $batch = install_profile_modules($install_state);
  $batch['title'] = t('Installing Infogentsia');
  return $batch;
}

/**
 * Implements hook_form_FORM_ID_alter() for install_configure_form().
 *
 * Allows the profile to alter the site configuration form.
 */
function infogentsia_installer_form_install_configure_form_alter(&$form, &$form_state) {
  
  // No default provided will force admin to enter a value here.
  $form['site_information']['site_name']['#default_value'] = '';
  
  // Pre-populate the default country with US
  $form['server_settings']['site_default_country'] = array( 
    '#type' => 'hidden',
    '#value' => 'US'
  );

  // Remove options to get updates
  unset($form['update_notifications']);

  // Collect site contact phone number
  $form['site_information']['site_phone_number'] = array(
    '#type' => 'textfield',
    '#title' => t('Contact Phone Number for users'),
    '#default_value' => variable_get('site_phone_number','+1 503.701.8747'),
    '#description' => t('What phone number would you like displayed at the bottom of the page?'),
  );

  // Collect site logo during installation
  $form['site_information']['site_logo'] = array(
    '#type' => 'file',
    '#title' => t('Site Logo'),
    '#description' => t('Upload a file, allowed extensions: jpg, jpeg, png, gif'),
  );

  $palettes = array('sq'=>'Square','grayscale'=>'Grayscale');  
  $form['site_information']['palette'] = array(
    '#type' => 'select',
    '#title' => t('Color palette'),
    '#options' => $palettes,
    '#default_value' => 'grayscale',
    '#description' => t('What color palette to use?')
  );

  $form['admin_account']['account']['fname'] = array('#type' => 'textfield',
    '#title' => st('First Name'),
    '#maxlength' => USERNAME_MAX_LENGTH,
    '#description' => st('Spaces are allowed; punctuation is not allowed except for periods, hyphens, and underscores.'),
    '#required' => TRUE,
    '#weight' => -12,
    '#attributes' => array('class' => array('username')),
  );
  $form['admin_account']['account']['lname'] = array('#type' => 'textfield',
    '#title' => st('Last Name'),
    '#maxlength' => USERNAME_MAX_LENGTH,
    '#description' => st('Spaces are allowed; punctuation is not allowed except for periods, hyphens, and underscores.'),
    '#required' => TRUE,
    '#weight' => -11,
    '#attributes' => array('class' => array('username')),
  );


  $form['#validate'][] = '_infogentsia_installer_logo_validate';
  $form['#submit'][] = '_infogentsia_installer_logo_submit';
  $form['#submit'][] = '_infogentsia_installer_update_users';
  $form['#submit'][] = '_infogentsia_installer_update_palette';
}

function _infogentsia_install_finished_override(&$install_state) {
  // Log out current user
  global $user;
  if ($user->uid) {
    module_invoke_all('user_logout', $user);
  }
  // Log in new user
  if ($account = user_load(2)) {
    $old_uid = $user->uid;
    $user = $account;
    $user->timestamp = time() - 9999;
    $edit = array();
    user_module_invoke('login', $edit, $user);
  }

  $defaultoutput = install_finished($install_state);

  // Update list (again for some reason)
  $list = variable_get('infogentsia_newsletters_list','');
  $num_updated = db_update('mailchimp_signup')
  ->fields(array(
    'mc_lists' => serialize(array($list=>$list)),
  ))
  ->condition('mcs_id', 1)
  ->execute();
  
  $output = _infogentsia_installer_load_text('finished.txt',true);
  drupal_set_title(t('Infogentsia installation complete'));
  if ($output) return $output;
  return '';
}

function _infogentsia_installer_logo_validate($form, &$form_state) {
  $file = file_save_upload('site_logo', array(
    'file_validate_is_image' => array(),
    'file_validate_extensions' => array('png gif jpg jpeg'),
  ));
  // If the file passed validation:
  if ($file) {
    // Move the file into the Drupal file system.
    if ($file = file_move($file, 'public://')) {
      // Save the file for use in the submit handler.
      $form_state['storage']['site_logo'] = $file;
    }
    else {
      form_set_error('site_logo', t("Failed to write the uploaded file to the site's file folder."));
    }
  }
  else {
    form_set_error('site_logo', t('No file was uploaded.'));
  }
}

function _infogentsia_installer_logo_submit($form, &$form_state) {  
  $file = $form_state['storage']['site_logo'];
  // We are done with the file, remove it from storage.
  unset($form_state['storage']['site_logo']);
  // Make the storage of the file permanent.
  $file->status = FILE_STATUS_PERMANENT;
  // Save file status.
  file_save($file);
  // Set a response to the user.
  $theme_sq_settings = variable_get('theme_sq_settings',array());
  $theme_sq_settings['logo_path'] = 'public://' . $file->filename;
  variable_set('theme_sq_settings',$theme_sq_settings);
  // Save site phone number
  variable_set('site_phone_number',$form_state['values']['site_phone_number']);
}

function _infogentsia_installer_set_logo() {
  $theme_settings = array(
    'toggle_logo' => 1,
    'toggle_name' => 1,
    'toggle_slogan' => 1,
    'toggle_node_user_picture' => 1,
    'toggle_comment_user_picture' => 1,
    'toggle_comment_user_verification' => 1,
    'toggle_favicon' => 1,
    'toggle_main_menu' => 1,
    'toggle_secondary_menu' => 1,
    'default_logo' => 0,
    'logo_path' => '/profiles/infogentsia_installer/themes/sq/img/logo.jpg',
    'logo_upload' => '',
    'default_favicon' => 0,
    'favicon_path' => '/profiles/infogentsia_installer/themes/sq/img/favicon.ico',
    'favicon_upload' => '',
  );
  variable_set('theme_settings',$theme_settings);
}

function _infogentsia_installer_update_palette($form, &$form_state) {
  $theme_sq_settings = variable_get('theme_sq_settings',array());
  $theme_sq_settings['tm_value_17'] = $form_state['values']['palette'];
  variable_set('theme_sq_settings',$theme_sq_settings);
}