<fieldset>
<p>Finally, you need to generate an API key that you will provide so we can connect to MailChimp and send messages on your behalf.</p>
<ol>
<li>Click on your name and click Account in the fly out menu.</li>
<li>Under Extras, click API Keys.</li>
<li>Click on Create A Key.</li>
<li>Copy the API Key into the field below.</li>
</ol>
<img width="480px" src="/profiles/infogentsia_installer/img/mailchimp_api.PNG" style="box-shadow: 10px 10px 5px #888888;border:1px solid black;"/>
</fieldset>