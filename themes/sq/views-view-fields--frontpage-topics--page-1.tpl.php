<?php global $base_url; $iurl = $base_url.'/'.drupal_get_path('theme','sq').'/'; ?>
<div id="term-<?php print $fields['tid']->content; ?>" class="term-<?php print $fields['tid']->content; ?> term pmason">
  <div class="blog-content">
    <div class="blog-thumbs-animate">
      <?php print $fields['field_content_image']->content; ?>
      <a href="<?php print $fields['path']->content; ?>"><p class="flex-caption"><?php print $fields['name']->content; ?></p></a>
      <div class="bhover">
        <a href="<?php print $fields['path']->content; ?>">
          <div class="inside">
            <h3><?php print $fields['name']->content; ?></h3>          
            View more in <?php print $fields['name']->content; ?>
          </div>
        </a>
      </div>
    </div>
  </div>
</div>