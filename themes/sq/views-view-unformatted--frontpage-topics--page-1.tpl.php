<?php 
$n1 = '';
foreach ($rows as $id => $row) {
  $n1 .= $row;
}
// Hard code another 
$n1 .= '<div id="term-all" class="term-all term pmason masonry-brick">
  <div class="blog-content">
    <div class="blog-thumbs-animate">
      <div class="field-content">
        <a href="/alltopics">
          <div class="inside">
            View a list of all the topics available.
          </div>
        </a>      
      </div>
      <a href="/alltopics"><p class="flex-caption">View All Topics</p></a>
      <div class="bhover">
        <a href="/alltopics">
          <div class="inside">
            View a list of all the topics available.
          </div>
        </a>
      </div>
    </div>
  </div>
</div>';

print ($n1 ? '<section class="blog-list"><div class="blist">'.$n1.'</div></section>' : '');
?>