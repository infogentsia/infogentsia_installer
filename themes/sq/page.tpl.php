<?php 
print render($page['header']); 
global $base_url;
$t = TRUE;
$iurl = $base_url.'/'.drupal_get_path('theme','sq').'/';
if (arg(1)) $arg1 = arg(1); else $arg1 = 0;
if (!(isset($page['content']['system_main']['nodes'][$arg1]) and isset($page['content']['system_main']['nodes'][$arg1]['#node']->type))) {
  $page['content']['system_main']['nodes'][$arg1]['#node'] = new stdClass;
  $page['content']['system_main']['nodes'][$arg1]['#node']->type = '';
}
if (isset($page['content']['system_main']['nodes'][$arg1]['field_type']['#items'][0]['value']) and $page['content']['system_main']['nodes'][$arg1]['field_type']['#items'][0]['value']) { 
  $ptype = $page['content']['system_main']['nodes'][$arg1]['field_type']['#items'][0]['value'];
} else {
  $ptype = '';
}
// Set up as group home page
$home_url = $front_page;
$logo_text = $site_name;
$fb = theme_get_setting('tm_value_1');
$tw = theme_get_setting('tm_value_0');
$li = strtolower(theme_get_setting('tm_value_16'));
// Account for users not putting the leading domain
if (!strpos($li,'linkedin.com')) $li = 'http://www.linkedin.com/' . $li;
$ig = url('node/4');
$logo_path = theme_get_setting('logo_path');
// Inject style into path of logo
$logo = file_create_url(image_style_url('logo', $logo_path));
?>
	<!--CONTAINER-->
	<div id="container">
		<!--HEADER-->
		<header>
			<!--LOGO, MENU AND SOCIAL-->			
			<div class="clear">
				<!--LOGO-->
				<div class="logo">
					<h1>
                        <span id="centerdummy"></span>
                        <a href="<?php print check_url($home_url); ?>" title="<?php print $logo_text; ?>" rel="home" id="logo">
                            <img src="<?php print $logo; ?>" alt="<?php print $logo_text; ?>" />
                            <?php if (theme_get_setting('display_site_name') == 1) {?>
                            <span class="flex-caption"><?php print variable_get('site_name','Infogentsia'); ?></span>
                            <? } ?>
                        </a>
                    </h1>
				</div>	
				<!--MENU-->
				<nav id="nav-menu">
					<div id="dropdown" class="menu clear">
					  <?php echo render($page['sidebar_top_menu']); ?>
					</div><select id="mselect"></select>
				</nav>	
				<!--SOCIAL-->	
				<div class="sociable">
					<ul>
						<li>
							<a href="<?php print 'http://twitter.com/'.$tw; ?>" class="twitter">
								<img src="<?php print $iurl; ?>img/twitter.png" alt="Twitter" title="Twitter" />
							</a>
						</li>
						<li>
							<a href="<?php print $li; ?>" class="linkedin">
								<img src="<?php print $iurl; ?>img/linkedin.png" alt="LinkedIn" title="LinkedIn" />
							</a>
						</li>
						<li class="social-width-2">
                            <?php
                            /* NOT BEING USED
							<a href="<?php print theme_get_setting('tm_value_2'); ?>" ><?php print t('Contact Us'); ?></a>
                            */ 
                            ?>
                            <a href="mailto:Nathan.Schmitt@infogentsia.com?subject=<?php print str_replace('+','%20',urlencode(t('Nathan, with infogentsia, is happy to answer your questions!')));?>" ><?php print t('Contact Us'); ?></a>
						</li>
					</ul>
				</div>	
			</div>	
		</header>
  <?php if (isset($messages) and $messages) { print '<section class="mmessages clear">'.$messages.'</section>'; } ?>
  <?php if ($ptype == 'big') { ?>
    <section class="about clear">
  <?php } elseif ($ptype == 'notitle') { ?>
    <section class="services node-content">
  <?php } else { ?>
    <section class="blog clear">
  <?php } ?>
      <?php print '<div id="search_region">' . render($page['search']) . '</div>'; ?>
	  <?php if(
	    $title and 
	    $ptype != 'notitle'
	    ) { 
	      if($tabs and $tabs['#primary'] and $page['content']['system_main']['nodes'][$arg1]['#node']->type != 'article') { print ''.str_replace('</li>',' </li> ',render($tabs)).''; } 
	      print '<h2 class="page-title">'.$title.'</h2>';
	      $t = FALSE;
	      } ?>
        <?php
          print '<div class="submenu_region submenu_top">';
	      print '<div class="subscribe_region">' . render($page['subscribe']) . '</div>';	      
          print '</div>';
        ?>
    <?php if ($page['content']['system_main']['nodes'][$arg1]['#node']->type) { ?>
	    <?php if (
	        $ptype != 'full' and 
	        $ptype != 'notitle' and
	        $ptype != 'big' and 
	        $page['content']['system_main']['nodes'][$arg1]['#node']->type != 'article' and
	        $sidebar = render($page['sidebar'])
	      ) { ?>
	  
	      <?php if ($ptype == 'left') { ?>
	        <div class="right left-content">
		        <?php print render($page['content']); ?>
		      </div>
		      <div class="left sidebar">
		        <?php print $sidebar; ?>
		      </div>
		    <?php } else {?>
		      <div class="left-content node-content">
		        <?php if($t and $tabs and $tabs['#primary']) { print ''.str_replace('</li>',' </li> ',render($tabs)).''; } ?>
		        <?php print render($page['content']); ?>
		      </div>
		      <div class="sidebar">
		        <?php print $sidebar; ?>
		      </div>
		    <?php } ?>
		  
		  <?php } elseif ($ptype == 'notitle') { ?>
		    <?php if($t and $tabs and $tabs['#primary']) { print ''.str_replace('</li>',' </li> ',render($tabs)).''; } ?>
		    <div class="clear">
		      <?php print render($page['content']); ?>
		    </div>
		  <?php } else {?>
		    <?php print render($page['content']); ?>
		  <?php } ?>
	
	  <?php } else {?>
	    <?php if (
	        arg(0) != 'frontpage' and
	        $sidebar = render($page['sidebar']) and 
            arg(0) != 'taxonomy' and
            arg(1) != 'term'  
	      ) { ?>
		    <div class="left-content node-content">
		      <?php if($t and $tabs and $tabs['#primary']) { print ''.str_replace('</li>',' </li> ',render($tabs)).''; } ?>
		      <?php if ($action_links) {print '<ul class="tabs">'.render($action_links).'</ul>';}?>
		      <?php print render($page['content']); ?>
		    </div>
		    <div class="sidebar">
		      <?php print $sidebar; ?>
		    </div>
		  <?php } else {?>
		    <?php print render($page['content']); ?>
		  <?php } ?>
	  <?php } ?>
	
    <?php	      
    // Display only on topic pages.
    if (arg(0) == 'taxonomy' && arg(1) == 'term' && is_numeric(arg(2))) {
        print '<div class="submenu_region submenu_region_bottom">';
        print '<div class="subscribe_region bottom_subscribe">' . render($page['subscribe']) . '</div>'; 
        print '</div">';
    }
    ?>
  </section>
  <div class="clr force-footer"></div>
<footer>
      <div class="copyright">
    <?php print render($page['footer_copyright']); ?>
  </div>
</footer>  
</div>