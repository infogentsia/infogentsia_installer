<?php
// $Id$
function sq_preprocess_html(&$variables) {
  global $base_url, $language;
  if (isset($_GET['response_type']) && $_GET['response_type'] == 'embed') {
    $variables['theme_hook_suggestions'][] = 'html__embed';
  }
  // Set palette for theme
  $theme_sq_settings = variable_get('theme_sq_settings',array());
  // Get the palette, defaulting to grayscale
  $palette = (isset($theme_sq_settings['tm_value_17'])) ? $theme_sq_settings['tm_value_17'] : 'grayscale';

  drupal_add_css(drupal_get_path('theme', 'sq') . '/css/' . $palette . '/palette.css', array('group' => CSS_THEME, 'every_page' => TRUE,'type' => 'file'));
  drupal_add_css(drupal_get_path('theme', 'sq') . '/css/' . $palette . '/responsive_palette.css', array('group' => CSS_THEME, 'every_page' => TRUE,'type' => 'file'));
}

function sq_css_rep($string) {
  if ($last_space = strrpos($string, ':')) {
    $string = substr($string, 0, $last_space);
  }
  return $string;
}
function sq_truncate_utf8($string, $len, $wordsafe = FALSE, $dots = FALSE, &$ll = 0) {

  if (drupal_strlen($string) <= $len) {
    return $string;
  }

  if ($dots) {
    $len -= 4;
  }

  if ($wordsafe) {
    $string = drupal_substr($string, 0, $len + 1); // leave one more character
    if ($last_space = strrpos($string, ' ')) { // space exists AND is not on position 0
      $string = substr($string, 0, $last_space);
      $ll = $last_space;
    }
    else {
      $string = drupal_substr($string, 0, $len);
	  $ll = $len;
    }
  }
  else {
    $string = drupal_substr($string, 0, $len);
	$ll = $len;
  }

  if ($dots) {
    $string .= '...';
  }

  return $string;
}

function sq_menu_link(array $variables) {
  $element = $variables['element'];
  $sub_menu = '';
  
  $element['#attributes']['class'][] = 'level-' . $element['#original_link']['depth'];
  if ($element['#below']) {
    $sub_menu = drupal_render($element['#below']);
  }
  $output = l($element['#title'], $element['#href'], $element['#localized_options']);
  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}

function sq_preprocess_page(&$variables) {
  if (isset($_GET['response_type']) && $_GET['response_type'] == 'embed') {
    $variables['theme_hook_suggestions'][] = 'page__embed';
  }
  sq_removetab('Tokens', $variables);
}

function sq_removetab($label, &$vars) {
  // Remove from primary tabs
  $i = 0;
  if (is_array($vars['tabs']['#primary'])) {
    foreach ($vars['tabs']['#primary'] as $primary_tab) {
      if ($primary_tab['#link']['title'] == $label) {
        unset($vars['tabs']['#primary'][$i]);
      }
      ++$i;
    }
    if (count($vars['tabs']['#primary']) < 2 ) { $vars['tabs']['#primary'] = array(); }    
  }
}
