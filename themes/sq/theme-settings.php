<?php

function sq_form_system_theme_settings_alter(&$form, $form_state) {

  $form['advansed_theme_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advanced Theme Settings'),
  );

  $form['advansed_theme_settings_social'] = array(
    '#type' => 'fieldset',
    '#title' => t('Social'),
    '#collapsible' => TRUE,
		'#collapsed' => FALSE,
  );
  $palettes = array('sq'=>'Square','grayscale'=>'Greyscale');  
  $form['advansed_theme_settings_social']['tm_value_17'] = array(
    '#type' => 'select',
    '#title' => t('Color palette'),
    '#options' => $palettes,
    '#default_value' => theme_get_setting('tm_value_17'),
    '#description' => t('What color palette to use?')
  );
  $form['advansed_theme_settings_social']['tm_value_0'] = array(
    '#type' => 'textfield',
    '#title' => t('Twitter'),
    '#default_value' => theme_get_setting('tm_value_0'),
    '#size' => 32,
  );
  $form['advansed_theme_settings_social']['tm_value_16'] = array(
    '#type' => 'textfield',
    '#title' => t('LinkedIn'),
    '#default_value' => theme_get_setting('tm_value_16'),
    '#size' => 32,
  );
  $form['advansed_theme_settings_social']['tm_value_16'] = array(
    '#type' => 'textfield',
    '#title' => t('LinkedIn'),
    '#default_value' => theme_get_setting('tm_value_16'),
    '#size' => 32,
  );
    // Display site name
  $form['advansed_theme_settings_social']['display_site_name'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display the site name with the logo?'),
    '#default_value' => theme_get_setting('display_site_name'),
    '#description' => t('Would you like to display the site name with the logo?'),
  );
}
