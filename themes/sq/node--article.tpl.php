 <?php

/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct url of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type, i.e., "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type, i.e. story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode, e.g. 'full', 'teaser'...
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined, e.g. $node->body becomes $body. When needing to access
 * a field's raw values, developers/themers are strongly encouraged to use these
 * variables. Otherwise they will have to explicitly specify the desired field
 * language, e.g. $node->body['en'], thus overriding any language negotiation
 * rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 */

$title = html_entity_decode($title,ENT_QUOTES); 

global $base_url, $user;
$iurl = $base_url.'/'.drupal_get_path('theme','sq').'/';
$domain = '';
preg_match_all('/http[s]*:\/\/([^\/]*)\/.*/i', $node->field_article_url['und'][0]['value'], $domain, PREG_PATTERN_ORDER);
$sourceDomain = implode('',$domain[1]);

?>
<?php if (!$page) { ?>
<div id="node-<?php print $node->nid; ?>" class="left-content <?php print $zebra.' '.$classes; ?> clearfix"<?php print $attributes; ?>>	
  <?php print render($title_prefix); ?><?php print render($title_suffix); ?>
	<div class="blog-details">
		<div class="date-cat">
			<span class="d"><?php print format_date($node->created,'blog'); ?></span>
            <?php if (render($content['field_article_author']) != '') { ?>
		    <label><?php print t(' by !author', array('!author' => render($content['field_article_author']))); ?></label>
            <?php } ?>
		</div>
	</div>
	<div class="article-desc">        
		<h3><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h3>
        <div class="article-domain">Source: <?php print $sourceDomain; ?></div>
		<?php
      hide($content['comments']);
      hide($content['links']);
      hide($content['field_article_author']);
      hide($content['field_article_url']);
      hide($content['field_topic']);
      hide($content['print_links']);
      print render($content);
    ?>
  </div>
</div>	
<?php } else { ?>
<div>
<div id="node-<?php print $node->nid; ?>" class="<?php print $zebra.' '.$classes; ?> node-teaser contextual-links-region clearfix"<?php print $attributes; ?>>	
	<div class="blog-details article-details">
		<div class="date-cat">
			<span class="d"><?php print format_date($node->created,'blog'); ?></span>
            <?php if (count($node->field_article_author) > 0 ) {?>
			    <label><?php print t(' by !author', array('!author' => render($content['field_article_author']))); ?></label>
            <?php }?>
		</div>
 		<div class="bcomment">
            <div class="comments"><h3>
            <?php print render($content['field_topic']); ?>                
                </h3></div>
		</div>
		<div class="share clear">
			<ul>
				<li>
					<script>
                        function li_click() {
                            u=location.href;
                            t=document.title;
                            window.open('http://www.linkedin.com/shareArticle?mini=true&url='+encodeURIComponent(u)+'&title='+encodeURIComponent(t)+'&source='+encodeURIComponent("<?php print htmlspecialchars(variable_get('site_name','Infogentsia'),ENT_QUOTES); ?>"),'sharer','toolbar=0,status=0,width=520,height=570');
                            return false;
                        }
                    </script>
					<a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php print url('node/'.$node->nid, array('absolute' => 1)); ?>&title=<?php print urlencode($node->title); ?>&source=<?php print urlencode(variable_get('site_name','Infogentsia')); ?>" onclick="return li_click()" target="_blank" class="facebook">
						<img src="<?php print $iurl; ?>img/linkedin.png" alt="LinkedIn" title="LinkedIn" />
					</a>
				</li>
			    <li>
				    <script>function twit_click() {u=location.href;t=document.title;window.open('http://twitter.com/share?url='+encodeURIComponent(u)+'&text='+encodeURIComponent(t),'share','toolbar=0,status=0,width=626,height=436');return false;}</script>
					<a href="http://twitter.com/share?url=<?php print url('node/'.$node->nid, array('absolute' => 1)); ?>&amp;text=<?php print $title; ?>" onclick="return twit_click()" target="_blank" class="twitter">
						<img src="<?php print $iurl; ?>img/twitter.png" alt="Twitter" title="Twitter" />
					</a>
				</li>
                <li id="mailtoblock">
                    <?php 
                        $mail  = 'subject=' . (($user->name == '') ? 'Someone' : $user->name). ' would like to share an article from ' . variable_get('site_name','Infogentsia');
                        //$mail .= '&body=' . $title . urlencode("\n\n") . htmlspecialchars(drupal_html_to_text(render($content['body'])),ENT_QUOTES) . urlencode("\n\n") . url(drupal_get_path_alias('node/'.$node->nid),array('absolute' => true));                        
                        $mail .= '&body=' . $title . urlencode("\n\n") . htmlspecialchars(preg_replace("/\r|\n/", urlencode("\n"),str_replace('%','%25',truncate_utf8(drupal_html_to_text(render($content['body'])),400,TRUE,TRUE,200))),ENT_QUOTES) . urlencode("\n\n") . url(drupal_get_path_alias('node/'.$node->nid),array('absolute' => true));                        
                        show($content['body']);
                    ?>
					<a class="share_by_email" href="mailto:?<?php print $mail; ?>">
						<img src="/profiles/infogentsia_installer/themes/sq/img/email.png" alt="Share by Email" title="Share by Email" />
					</a>				
                </li>
			</ul>
		</div>			
	</div>

	<div class="blog-desc article-inner-desc">
      <div class="article-domain">Source: <?php print $sourceDomain; ?></div>
		<?php
      hide($content['comments']);
      hide($content['links']);
      hide($content['field_article_url']);
      hide($content['field_article_author']);      
      hide($content['field_image']);
      hide($content['field_topic']);
      hide($content['print_links']);
      print render($content);
    ?>
  <div class="article-read-full"><a href="<?php print $node->field_article_url['und'][0]['value'] ?>" rel="nofollow" target="_blank">read original article</a></div>
  </div>
  <div class="clear"></div>
</div>
<div class="clear"></div>
<a name="comment_section"></a>
<?php print render($content['comments']); ?>	
</div>
  
<?php } ?>
