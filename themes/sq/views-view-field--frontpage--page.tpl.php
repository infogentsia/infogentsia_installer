<?php global $base_url; $iurl = $base_url.'/'.drupal_get_path('theme','sq').'/'; ?>
<div id="node-<?php print $fields['nid']->content; ?>" class="node-<?php print $fields['nid']->content; ?> node pmason">
	<div class="blog-content">
		<div class="blog-thumbs-animate">
            <?php if ($fields['type']->raw == 'simpleads')  { ?>
			<a href="/simpleads/redirect/<?php print $fields['nid']->raw; ?>">
                <?php print $fields['field_ad_image']->content; ?>
            </a>
            <?php
            _simpleads_increase_impression(node_load($fields['nid']->raw));
            ?>
            <?php } elseif ($fields['type']->raw == 'topic') {?>
			<?php print $fields['field_content_image']->content; ?>
            <p class="flex-caption"><?php print $fields['title']->content; ?></p>
			<div class="bhover">
				<div class="inside">
                    <?php print $fields['view']->content ?>
					<div class="action">						
						<a href="<?php print $fields['uri']->content; ?>" title="" class="zoom" rel="lightbox[field_content_image]">
                            <img src="<?php print $iurl; ?>img/zoom.png" alt="" title="" />
						</a>
                        <a href="<?php print $fields['path']->content; ?>">
                            <img src="<?php print $iurl; ?>img/link.png" alt="" title="" />
                        </a>
                    </div>
				</div>
			</div>
            <?php } ?>
		</div>
	</div>
</div>