var $j = jQuery.noConflict();

$j(window).load(function() {

	$j(".widget-partners ul li a img").css({opacity: 0.5});
	$j(".members ul li img").css({opacity: 0.3});
	$j(".slist img").css({opacity: 0.7});
	
	/*HOME GRID*/

	$j(".blog-thumbs-animate").hover(function(){
		$j("img", this).stop().animate({opacity: 1},{queue:false,duration:75});
		$j(".bhover", this).animate({opacity: 1},{queue:false,duration:75});	
        $j(".flex-caption", this).animate({opacity: 0},{queue:false,duration:75});	
	}, function() {
		$j(".bhover", this).animate({opacity: 0},{queue:false,duration:75});		
        $j(".flex-caption", this).animate({opacity: 1},{queue:false,duration:75});		
	});

	/*PARTNERS*/

	$j(".widget-partners ul li a").hover(function(){
		$j("img", this).stop().animate({opacity: 1},{queue:false,duration:75});
	}, function() {
		$j("img", this).stop().animate({opacity: 0.5},{queue:false,duration:75});
	});

	/*MEMBERS*/

	$j(".members ul li").hover(function(){
		$j("img", this).stop().animate({opacity: 1},{queue:false,duration:75});
		$j(".mdetails", this).animate({opacity: 1},{queue:false,duration:75});	
	}, function() {
		$j("img", this).stop().animate({opacity: 0.5},{queue:false,duration:75});
		$j(".mdetails", this).animate({opacity: 0},{queue:false,duration:75});		
	});

	/*BLOG LIST*/

	$j(".blog-image").hover(function(){
		$j("img", this).stop().animate({opacity: 0.7},{queue:false,duration:75});
		$j(".blog-hover", this).animate({opacity: 1},{queue:false,duration:75});	
	}, function() {
		$j("img", this).stop().animate({opacity: 1},{queue:false,duration:75});
		$j(".blog-hover", this).animate({opacity: 0},{queue:false,duration:75});		
	});

	/*SERVICES LIST*/

	$j(".slist").hover(function(){
		$j("img", this).stop().animate({opacity: 1},{queue:false,duration:75});
	}, function() {
		$j("img", this).stop().animate({opacity: 0.5},{queue:false,duration:75});		
	});

});