var $j = jQuery.noConflict();

$j('div.menu').find('li:has(ul)').addClass('has-menu');

// Create default option "Go to..."
$j(window).load(function () {

    $j('#mselect').change(function () {
        window.location = $j(this).val();
    });

    $j("<option />", {
        "selected": "selected",
        "value": "",
        "text": 'Select Page'
    }).appendTo("#mselect");

    $j('#nav-menu ul.menu').find('li').each(function () {
        cur_link = $j(this).children("a");
        iii = 0;
        $j(this).parents('ul.menu').each(function () { iii++; });
        var str = new Array(iii).join('--');
        // Replace /tile with /list in select menu links
        target = cur_link.attr("href");
        view = target.substr(target.length - 5, 5).toLowerCase();
        if (view == '/tile') {
            target = target.substring(0, target.length - 5) + '/list';
        }
        $j('#mselect').append('<option value=' + target + ' >' + str + cur_link.text() + '</option>');
        cur_link.append('<span class="menu-description">' + cur_link.attr("title") + '</span>');
    });

});

