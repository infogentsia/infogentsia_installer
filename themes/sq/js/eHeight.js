var $j = jQuery.noConflict();

$j(document).ready(function(){
	
	/*FOOTER WIDGETS*/

setTimeout(function(){
	var biggestHeight = 0;
	
	$j('.footer-widgets .widget').each(function(){
		if($j(this).height() > biggestHeight){
			biggestHeight = $j(this).height();
		}
	});
	$j('.footer-widgets .widget').height(biggestHeight);


	/*RELATED POSTS*/

	var tallestHeight = 0;

	$j('.rpost-inside').each(function(){
		if($j(this).height() > tallestHeight){
			tallestHeight = $j(this).height();
		}
	});
	$j('.rpost-inside').height(tallestHeight);
},1000);

});