<?php
/*
 * CONFIGURE MAILCHIMP
 */
function _infogentsia_installer_mailchimp() {
  drupal_set_title(t('Configure MailChimp'));
  $form['instructions'] = array(
    '#markup' => t(_infogentsia_installer_load_text('mailchimp.txt'))
  );
  $form['instructions_list'] = array(
    '#markup' => t(_infogentsia_installer_load_text('mailchimp_list.txt'))
  );  
  $form['instructions_api'] = array(
    '#markup' => t(_infogentsia_installer_load_text('mailchimp_api.txt'))
  );  
  $form['mailchimp_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('MailChimp API Key'),
    '#description' => t('The mailchimp API key to use to send newsletters from this site.'),
    '#default_value' => variable_get('mailchimp_api_key',''),
    '#maxlength' => 72,
    '#required' => TRUE,
    '#size' => 32
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save MailChimp API Key'),
  );
  return $form;
}

function _infogentsia_installer_mailchimp_submit($form, &$form_state) {
  $mailchimp_api_key = trim($form_state['values']['mailchimp_api_key']);
  variable_set('mailchimp_api_key', $mailchimp_api_key);
  drupal_set_message(t('MailChimp API key saved.'));
}