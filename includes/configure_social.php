<?php
/*
 * CONFIGURE SOCIAL ACCOUNTS
 */
function _infogentsia_installer_social() {

  $theme_sq_settings = variable_get('theme_sq_settings',array());  
  $linkedIn = (isset($theme_sq_settings['tm_value_16'])) ? $theme_sq_settings['tm_value_16'] : '';
  $twitter  = (isset($theme_sq_settings['tm_value_0']))  ? $theme_sq_settings['tm_value_0']  : '';

  drupal_set_title(t('Configure Social Accounts'));
  $form['instructions'] = array(
    '#markup' => t(_infogentsia_installer_load_text('social.txt'))
  );
  $form['twitter'] = array(
    '#type' => 'textfield',
    '#title' => t('Twitter User Name'),
    '#description' => t('Enter the Twitter user name for your company.'),
    '#default_value' => $twitter,
    '#maxlength' => 72,
    '#required' => FALSE,
    '#size' => 32
  );
  $form['linkedin'] = array(
    '#type' => 'textfield',
    '#title' => t('LinkedIn Company Profile'),
    '#description' => t('Enter the URL to your company\'s LinkedIn profile.'),
    '#default_value' => $linkedIn,
    '#maxlength' => 72,
    '#required' => FALSE,
    '#size' => 32
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update Social Accounts'),
  );
  $form['skip'] = array(
    '#type' => 'submit',
    '#value' => t('Skip Update'),
  );
  return $form;
}

function _infogentsia_installer_social_submit($form, &$form_state) {
  if ($form_state['values']['submit'] == t('Update Social Accounts')) {
    $theme_sq_settings = variable_get('theme_sq_settings',array());  
    if ($form_state['values']['linkedin'] != '') $theme_sq_settings['tm_value_16'] = $form_state['values']['linkedin'];
    if ($form_state['values']['twitter']  != '') $theme_sq_settings['tm_value_0']  = $form_state['values']['twitter'];
    variable_set('theme_sq_settings',$theme_sq_settings);  
    drupal_set_message(t('Social Accounts updated.'));
  }
}