<?php
function readDb($site) {    
  include_once('/var/www/sites/sites/' . $site . '/settings.php');
  return $databases['default']['default'];
}   

function getListConfig($site) {
  $db = readDb($site);

  // Log in to database to create new database and user
  $servername = "localhost";
  $conn = new mysqli($servername, $db['username'], $db['password']);
  // Check connection
  if ($conn->connect_error) {
	return false;
  } 
  mysqli_select_db($conn,$db['database']);
  $sql = "select * from variable where name = 'mailchimp_api_key' or name = 'infogentsia_newsletters_list'";
  $variables = array();
  $results = $conn->query($sql);
  while($row = mysqli_fetch_array($results)) { 
    $variables[$row['name']] = unserialize($row['value']);
  } 
  $conn->close();
  return $variables;
}  

function getListId($site) {
  $db = readDb($site);

  // Log in to database to create new database and user
  $servername = "localhost";
  $conn = new mysqli($servername, $db['username'], $db['password']);
  // Check connection
  if ($conn->connect_error) {
	return false;
  } 
  mysqli_select_db($conn,$db['database']);
  $sql = "select value from variable where name = 'infogentsia_newsletters_list'";
  $listid = '';
  $results = $conn->query($sql);
  while($row = mysqli_fetch_array($results)) { 
    $listid = unserialize($row['value']);
  } 
  $conn->close();
  return $listid;
}  

function getLists($site) {
  include_once('/var/www/sites/profiles/infogentsia_installer/libraries/mailchimp/src/Mailchimp.php');
  $config = getListConfig($site);
  $MailChimp = new MailChimp($config['mailchimp_api_key']);
  if (isset($config['infogentsia_newsletters_list'])) {
    $result = $MailChimp->lists->getList(array('list_id' => $config['infogentsia_newsletters_list']));
    return $result['data'][0]['name'];
  }
  return NULL;
}

function getUsedLists() {
  $sites = array();
  include_once('/var/www/sites/sites/sites.php');
  $usedLists = array();
  foreach ($sites as $site) {
    $newsletter = getLists($site);
    if (!is_null($newsletter)) $usedLists[$site] = $newsletter;
  }
  drupal_set_message($usedLists);
  return $usedLists;
}

function getUsedListIds() {
  include_once('/var/www/sites/sites/sites.php');
  $listids = array();
  foreach ($sites as $site) {
    $listids[$site] = getListId($site);
  }
  return $listids;
}