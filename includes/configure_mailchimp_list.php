<?php

function getListConfig($site) {
  include_once('/var/www/sites/sites/' . $site . '/settings.php');
  $db =  $databases['default']['default'];
  // Log in to database to create new database and user
  $conn = new mysqli('localhost', $db['username'], $db['password']);
  // Check connection
  if ($conn->connect_error) {
	return false;
  } 
  mysqli_select_db($conn,$db['database']);
  $sql = "select * from variable where name = 'mailchimp_api_key' or name = 'infogentsia_newsletters_list'";
  $variables = array();
  $results = $conn->query($sql);
  while($row = mysqli_fetch_array($results)) { 
    $variables[$row['name']] = unserialize($row['value']);
  } 
  $conn->close();
  return $variables;
}  

function getLists($site) {
  include_once('/var/www/sites/profiles/infogentsia_installer/libraries/mailchimp/src/Mailchimp.php');
  $config = getListConfig($site);
  $MailChimp = new MailChimp($config['mailchimp_api_key']);
  if (isset($config['infogentsia_newsletters_list'])) {
    $result = $MailChimp->lists->getList(array('list_id' => $config['infogentsia_newsletters_list']));
    return $result['data'][0]['name'];
  }
  return NULL;
}

function getUsedLists() {
  $sites = array();
  $sitefile = file_get_contents('/var/www/sites/sites/sites.php');
  if (!$sitefile) {  
    drupal_set_message('Not able to open /var/www/sites/sites/sites.php', 'err');
  } else {
    // Intend to only use this on .domains that are not .co.*
    $domainParts = explode('.',$_SERVER['HTTP_HOST']); 
    // Strip off last two segments of the domain
    unset($domainParts[count($domainParts)-1]);
    unset($domainParts[count($domainParts)-1]);
    // Put domain back together
    $currentDomain = trim(implode('.',$domainParts));
    $rows = explode("\n",$sitefile);
    foreach ($rows as $row) {
      if (strpos($row,"=")) {
	    $parts = explode("=",$row);
        $dir = preg_replace("/[^A-Za-z0-9_]/", '', $parts[1]);
        if (trim($dir) != $currentDomain) $sites[] = $dir;
      }
	}
  }

  $usedLists = array();  
  foreach ($sites as $site) {
    $newsletter = getLists($site);
    if (!is_null($newsletter)) $usedLists[$site] = $newsletter;
  }
  return $usedLists;
}

function _infogentsia_installer_mailchimp_list() {
  // Newsletter list
  $usedLists = getUsedLists();
  
  // Get lists and start building  
  $defaultList = NULL;
  $lists = array();
  $igs = array();
  foreach (mailchimp_get_lists() as $list) {
    $lists[$list['id']] = $list['name'];
    if (is_null($defaultList)) $defaultList = $list['id'];
  }

  // Check to see if there are even any lists
  if (count($lists) < 1) {
      drupal_set_message('No list was found.','error');
      $form['error_inst1'] = array(
        '#markup' => 'Please create a list within MailChimp and then refresh this page.'
      );
      $form['instructions_list'] = array(
        '#markup' => t(_infogentsia_installer_load_text('mailchimp_list.txt'))
      );  
      $form['error_inst2'] = array(
        '#markup' => '<a href="/install.php?profile=infogentsia_installer&locale=en" class="button">Refresh</a>'
      );
      return $form;
  } else if (count($lists) == 1) {
    $form['list_notice'] = array('#markup' => 'One list found. Will use ' . $lists[$defaultList] . ' list.');
    $form['infogentsia_newsletters_list'] = array(
      '#type' => 'hidden',
      '#value' => $defaultList
    );
  } else {
    $table = '<table><tr style="border-bottom:1px black solid;background:gray;"><td>Site</td><td>List</td></tr>';
    foreach($usedLists as $site => $list) {
      $table .= "<tr><td>$site</td><td>$list</td></tr>";
    }
    $table .= '</table>';
    $form['list_notice'] = array('#markup' => '<h3>CURRENTLY USED LISTS</<h3>' . $table);
    $form['infogentsia_newsletters_list'] = array(
      '#type' => 'select',
      '#title' => t('List'),
      //'#options' => array_merge(array('' => t('-- Select --')),$lists),
      '#options' => $lists,
      '#default_value' => $defaultList,
      '#description' => t('What MailChimp list will the campaigns and interest groups be created in. If no lists show, create one on the Lists and Users tab (/admin/config/services/mailchimp/lists).'),
    );
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save MailChimp List'),
  );
  return $form;
}

function _infogentsia_installer_mailchimp_list_submit($form, &$form_state) {
  $list = $form_state['values']['infogentsia_newsletters_list'];
  if ($list != '') {
      variable_set('infogentsia_newsletters_list', $list);
      drupal_set_message('MailChimp configuration updated.');
  } else {
      drupal_set_message('Unable to associate MailChimp list. Please contact Infogentsia for assistance.','error');
  }
}
