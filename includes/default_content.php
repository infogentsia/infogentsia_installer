<?php
function _infogentsia_installer_default_content() {
  
  // Create content
  $nodes = array();
  // Create terms of service page
  $node = _infogentsia_installer_create_node('tos.txt', 'page');
  $node->field_type['und'][0]['value'] = 'full';
  node_save($node);
  $nodes[1] = $node;

  // Create privacy policy page
  $node = _infogentsia_installer_create_node('pp.txt', 'page');
  $node->field_type['und'][0]['value'] = 'full';
  node_save($node);
  $nodes[2] = $node;

  // Create links for secondary menu
  for ($nid = 1; $nid <=2; $nid++) {
    _infogentsia_installer_menu_link($nodes[$nid]->title, drupal_get_path_alias('node/' . $nid), 'menu-secondary-links');
  }

  // Create contact form (nodes 3 and 4)
  _infogentsia_create_contact_form();
 
  // Create 404 page
  $node = _infogentsia_installer_create_node('404.txt', 'page');
  $node->field_type['und'][0]['value'] = 'full';
  node_save($node);

  // Set 404 page
  variable_set('site_404',drupal_get_path_alias('node/'.$node->nid));

  // Create 403 page
  $node = _infogentsia_installer_create_node('403.txt', 'page');
  $node->field_type['und'][0]['value'] = 'right';
  node_save($node);
  $path_vars = array('source'=>'node/'.$node->nid,'alias'=>'403');
  path_save($path_vars);


  // Set 403 page
  variable_set('site_403',drupal_get_path_alias('node/'.$node->nid));


  // Update block now that it has content
  $upd = db_update('block')
       ->fields(array(
         'region'=>'footer_copyright',
         'status'=> 1,
         'weight' => 3
       ))
       ->condition('delta','menu-secondary-links')
       ->condition('theme','sq')
       ->execute();
}


function _infogentsia_create_contact_form() {
  // Create contact form
  $node = _infogentsia_installer_create_node('contact.txt','webform');

  // Create the webform components.
  $components = array(
    array(
      'name' => 'Subject',
      'form_key' => 'subject',
      'type' => 'select',
      'mandatory' => 1,
      'weight' => 0,
      'pid' => 0,
      'value' => 'suggestion',
      'extra' => array(
        'title_display' => 'inline',
        'private' => 0,
        'items' => "issue|Website Issue\nadvertise|Advertising Inquiry\nsales|Sales inquiry\nsuggestion|Suggestion",
        'multiple' => 0,
        'aslist' => 1
      ),
    ),
    array(
      'name' => 'Share with us',
      'form_key' => 'body',
      'type' => 'textarea',
      'mandatory' => 1,
      'weight' => 10,
      'pid' => 0,
      'extra' => array(
        'private' => 0,
      ),
    ),
    array(
      'name' => 'Name',
      'form_key' => 'name',
      'type' => 'textfield',
      'mandatory' => 0,
      'weight' => 2,
      'pid' => 0,
      'extra' => array(
        'title_display' => 'inline',
        'private' => 0,
      ),
    ),
    array(
      'name' => 'Phone Number',
      'form_key' => 'phone_number',
      'type' => 'textfield',
      'mandatory' => 0,
      'weight' => 3,
      'pid' => 0,
      'extra' => array(
        'title_display' => 'inline',
        'private' => 0,
      ),
    ),
    array(
      'name' => 'Email',
      'form_key' => 'email',
      'type' => 'email',
      'mandatory' => 0,
      'weight' => 4,
      'pid' => 0,
      'extra' => array(
        'title_display' => 'inline',
        'private' => 0,
      ),
    ),
  );

  // Setup notification email.
  $emails = array(
    array(
      'email' => variable_get('site_mail','info@infogentsia.com'),
      'subject' => 'default',
      'from_name' => 3,
      'from_address' => 5,
      'template' => 'default',
      'excluded_components' => array(),
     ),
  );

  // Attach the webform to the node.
  $node->webform = array(
    'confirmation' => '',
    'confirmation_format' => NULL,
    'redirect_url' => '<confirmation>',
    'status' => '1',
    'block' => '0',
    'teaser' => '0',
    'allow_draft' => '0',
    'auto_save' => '0',
    'submit_notice' => '1',
    'submit_text' => '',
    'submit_limit' => '-1', // User can submit more than once.
    'submit_interval' => '-1',
    'total_submit_limit' => '-1',
    'total_submit_interval' => '-1',
    'record_exists' => TRUE,
    'roles' => array(
      0 => '1', // Anonymous user can submit this webform.
      1 => '2'
    ),
    'emails' => $emails,
    'components' => $components,
  );
  node_save($node);
  $theme_sq_settings = variable_get('theme_sq_settings',array());  
  $theme_sq_settings['tm_value_2'] = '/' . drupal_get_path_alias('node/3');  
  variable_set('theme_sq_settings',$theme_sq_settings);  

  // Create Infogtentsia contact form
  $node = _infogentsia_installer_create_node('igcontact.txt','webform');

  // Create the webform components.
  $components = array(
    array(
      'name' => 'Topics you are interested in:',
      'form_key' => 'topics',
      'type' => 'textarea',
      'mandatory' => 1,
      'weight' => 10,
      'pid' => 0,
      'extra' => array(
        'private' => 0,
      ),
    ),
    array(
      'name' => 'Name',
      'form_key' => 'name',
      'type' => 'textfield',
      'mandatory' => 0,
      'weight' => 2,
      'pid' => 0,
      'extra' => array(
        'title_display' => 'inline',
        'private' => 0,
      ),
    ),
    array(
      'name' => 'Phone Number',
      'form_key' => 'phone_number',
      'type' => 'textfield',
      'mandatory' => 0,
      'weight' => 3,
      'pid' => 0,
      'extra' => array(
        'title_display' => 'inline',
        'private' => 0,
      ),
    ),
    array(
      'name' => 'Email',
      'form_key' => 'email',
      'type' => 'email',
      'mandatory' => 0,
      'weight' => 4,
      'pid' => 0,
      'extra' => array(
        'title_display' => 'inline',
        'private' => 0,
      ),
    ),
  );

  // Setup notification email.
  $emails = array(
    array(
      'email' => 'info@infogentsia.com',
      'subject' => 'default',
      'from_name' => 3,
      'from_address' => 5,
      'template' => 'default',
      'excluded_components' => array(),
     ),
  );

  // Attach the webform to the node.
  $node->webform = array(
    'confirmation' => '',
    'confirmation_format' => NULL,
    'redirect_url' => '<confirmation>',
    'status' => '1',
    'block' => '0',
    'teaser' => '0',
    'allow_draft' => '0',
    'auto_save' => '0',
    'submit_notice' => '1',
    'submit_text' => '',
    'submit_limit' => '-1', // User can submit more than once.
    'submit_interval' => '-1',
    'total_submit_limit' => '-1',
    'total_submit_interval' => '-1',
    'record_exists' => TRUE,
    'roles' => array(
      0 => '1', // Anonymous user can submit this webform.
      1 => '2'
    ),
    'emails' => $emails,
    'components' => $components,
  );
  node_save($node);
}
