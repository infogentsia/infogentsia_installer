<?php
/*
 * CONFIGURE ANYTHING ELSE
 */
function _infogentsia_installer_configure() {  
  // Update auto_complete settings.
  $upd = db_update('search_autocomplete_forms')
         ->fields(array(
           'data_view'=>'custom_nodes_autocomplete',
           'data_callback'=> 'search_autocomplete/autocomplete/3/'
         ))
         ->condition('data_view','nodes_autocomplete')
         ->execute();
  
  // Set front page
  variable_set('site_frontpage','frontpage');

  // Create campaign
  $campaign_settings = array();
  $result = db_query("SELECT name FROM {variable} WHERE name LIKE 'infogentsia_newsletters%'");
  foreach ($result as $record) {
    $campaign_settings[$record->name] = variable_get($record->name,'');
  }
  // Do not pass a value to start the campaign as this should only be passed in $campaign_settings when it's from the admin form
  _infogentsia_update_campaign(true);
  
  // Rebuild all the menus
  menu_cache_clear_all(); 
}
