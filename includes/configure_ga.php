<?php
/*
 * CONFIGURE GA
 */
function _infogentsia_installer_googleanalytics() {
  drupal_set_title(t('Configure Google Analytics'));
  $form['instructions'] = array(
    '#markup' => t(_infogentsia_installer_load_text('ga.txt'))
  );
  $form['googleanalytics_account'] = array(
    '#type' => 'textfield',
    '#title' => t('Google Account ID'),
    '#description' => t('The Google Analytics account ID to use with this site.'),
    '#default_value' => variable_get('googleanalytics_account',''),
    '#maxlength' => 72,
    '#size' => 32
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Google Analytics Account ID'),
  );
  $form['skip'] = array(
    '#type' => 'submit',
    '#value' => t('Skip Google Analytics Configuration'),
  );
  return $form;
}
function _infogentsia_installer_googleanalytics_submit($form, &$form_state) {
  if ($form_state['values']['submit'] == t('Save Google Analytics Account ID') && $form_state['values']['googleanalytics_account'] != '') {
    module_enable(array('googleanalytics'));
    drupal_set_message(t('Enabled Google Analytics.'));
    $googleanalytics_account = trim($form_state['values']['googleanalytics_account']);
    variable_set('googleanalytics_account', $googleanalytics_account);

    drupal_set_message(t('Google Analytics Account ID saved.'));
    
    // Add GA permissions
    $roles = user_roles();
    $aid = array_search('administrator', $roles);
    user_role_grant_permissions($aid, array('opt-in or out of tracking','administer google analytics'));
  } 
}
