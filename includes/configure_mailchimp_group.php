<?php

function _infogentsia_installer_mailchimp_group() {
  
  // Get lists and start building  
  $defaultList = NULL;
  $lists = array();
  $igs = array();
  foreach (mailchimp_get_lists(array(variable_get('infogentsia_newsletters_list',''))) as $list) {
    if ($list['stats']['group_count']) $igs[$list['id']] = $list['intgroups'];
  }

  // Get interest groups
  $defaultigl = NULL;
  $options = array('create_new'=>'--Create new interest grouping--');
  $default_group = 'create_new';
  $interestGroupings = null;
  
  // Get the interest groups if they exist
  if (array_key_exists(variable_get('infogentsia_newsletters_list',''),$igs)) {
    $interestGroupings = $igs[variable_get('infogentsia_newsletters_list','')];
  } 

  if (is_array($interestGroupings)) {
    foreach($interestGroupings as $interestGrouping) {
      $options[$interestGrouping['id']] = $interestGrouping['name'];
      if (is_null($defaultigl)) $defaultigl = $interestGrouping['id'];
      if ($interestGrouping['name'] == 'Topics') {
         unset($options['create_new']);   
         $default_group = $interestGrouping['id'];
      }
    }
  }
  // Create default form
  $form['infogentsia_newsletters_interest_list_group'] = array(
    '#type' => 'select',
    '#title' => t('Interest Grouping'), 
    '#options' => $options,
    '#default_value' => variable_get('infogentsia_newsletters_interest_list_group',$defaultigl),
    '#description' => t('Select an existing interest grouping from MailChimp. This grouping needs to be created within MailChimp and have a group of "All Topics" created manually. All other groups will be maintained through this interface.')
  );
    //string subject
  $form['infogentsia_newsletters_subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Subject'),
    '#description' => t('The subject of the email you send out.'),
    '#default_value' => variable_get('infogentsia_newsletters_subject','News Briefings from ' . variable_get('site_name','Infogentsia') . ' for *|RSSFEED:DATE|*'),
    '#maxlength' => 72,
    '#required' => TRUE,
    '#size' => 32
  );
  //string from_email the From: email address for your campaign message
  $form['infogentsia_newsletters_from_email'] = array(
    '#type' => 'textfield',
    '#title' => t('From email'),
    '#description' => t('The From: email address for your campaign message.'),
    '#default_value' => variable_get('infogentsia_newsletters_from_email',variable_get('site_mail','')),
    '#maxlength' => 255,
    '#required' => TRUE,
    '#size' => 32
  );
  //string from_name the From: name for your campaign message (not an email address)
  $form['infogentsia_newsletters_from_name'] = array(
    '#type' => 'textfield',
    '#title' => t('From name'),
    '#description' => t('The From: name for your campaign message (not an email address).'),
    '#default_value' => variable_get('infogentsia_newsletters_from_name',variable_get('site_name','')),
    '#maxlength' => 255,
    '#required' => TRUE,
    '#size' => 32
  );   
  //string to_name the To: name recipients will see (not email address)
  $form['infogentsia_newsletters_to_name'] = array(
    '#type' => 'textfield',
    '#title' => t('To name'),
    '#description' => t('The To: name name recipients will see (not an email address).'),
    '#default_value' => variable_get('infogentsia_newsletters_to_name','*|FNAME|*'),
    '#maxlength' => 255,
    '#required' => TRUE,
    '#size' => 32
  );
  // Display site name
  $form['infogentsia_newsletters_start_campaign'] = array(
    '#type' => 'checkbox',
    '#title' => t('Start sending news briefings site creation?'),
    '#default_value' => FALSE,
    '#description' => t('If the email domain is not verified with MailChimp, this function will fail and the site will need to be deleted and recreated.'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save MailChimp Grouping'),
  );
  return $form;
}

function _infogentsia_installer_mailchimp_group_submit($form, &$form_state) {
  $ig = '';
  if ($form_state['values']['infogentsia_newsletters_interest_list_group'] != 'create_new') $ig = $form_state['values']['infogentsia_newsletters_interest_list_group'];
  if ($ig == '') {
      // Create grouping
      $mcapi = mailchimp_get_api_object();
      $response = $mcapi->lists->interestGroupingAdd(variable_get('infogentsia_newsletters_list',''), 'Topics', 'checkboxes', array('All Topics'));
      if ($response['id']) $ig = $response['id'];
  }
  if ($ig != '') {
      variable_set('infogentsia_newsletters_interest_list_group', $ig);
      drupal_set_message('MailChimp configuration updated.');
  } else {
      drupal_set_message('Unable to create/associate MailChimp interest group. Please contact Infogentsia for assistance.','error');
  }
  
  variable_set('infogentsia_newsletters_campaign_name',variable_get('site_name','Infogentsia'));
  variable_set('infogentsia_newsletters_analytics', array());
  // Set remaining infogentsia_newsletter values
  $tracking = array('opens'=>t('Opens'), 'html_clicks'=>t('HTML Clicks'), 'text_clicks'=>t('Text Clicks'));
  foreach ($tracking as $key=>$value) {
    variable_set('infogentsia_newsletters_' . $key, TRUE);
  }

  $analytics = array('google'=>t('Google Analytics'), 'clicktale'=>t('ClickTale'), 'gooal'=>t('Goo.al'));
  foreach ($analytics as $key=>$value) {
    variable_set('infogentsia_newsletters_' . $key, '');
  }

  variable_set('infogentsia_newsletters_authenticate', FALSE);
  variable_set('infogentsia_newsletters_auto_footer', FALSE);
  variable_set('infogentsia_newsletters_inline_css', TRUE);
  variable_set('infogentsia_newsletters_generate_text', TRUE);
  variable_set('infogentsia_newsletters_subject',$form_state['values']['infogentsia_newsletters_subject']);
  variable_set('infogentsia_newsletters_from_email',$form_state['values']['infogentsia_newsletters_from_email']);
  variable_set('infogentsia_newsletters_from_name',$form_state['values']['infogentsia_newsletters_from_name']);
  variable_set('infogentsia_newsletters_to_name',$form_state['values']['infogentsia_newsletters_to_name']);
  variable_set('infogentsia_newsletters_template_type','user');
  
  // Import/Create the default templates
  $theme_sq_settings = variable_get('theme_sq_settings',array());
  
  $header = '<img style="max-width: 350px;max-height: 170px;" src="*|LOGO|*"><br /><h3>*|SITE_NAME|*</h3>';
  $header_template = _infogentsia_installer_load_text('nl_temp_header.txt');
  $content_template = _infogentsia_installer_load_text('nl_temp_content.txt');
  $footer_template = _infogentsia_installer_load_text('nl_temp_footer.txt');
  
  // Make sure we didn't import null values
  if ($header)           variable_set('infogentsia_newsletters_header',$header);
  if ($header_template)  variable_set('infogentsia_newsletters_topic_header_template',$header_template);
  if ($content_template) variable_set('infogentsia_newsletters_topic_content_template',$content_template);
  if ($footer_template)  variable_set('infogentsia_newsletters_topic_footer_template',$footer_template);
  variable_set('infogentsia_newsletters_footer','');

  variable_set('infogentsia_newsletters_topic_count','4');
  variable_set('infogentsia_newsletters_header_loc','header');
  variable_set('infogentsia_newsletters_content_loc','content');
  variable_set('infogentsia_newsletters_footer_loc','footer');

  variable_set('infogentsia_newsletters_schedule','daily');
  variable_set('infogentsia_newsletters_schedule_hour','5');
  variable_set('infogentsia_newsletters_days',array('1','2','3','4','5','6','7'));

  // Import and create template
  $mcapi = mailchimp_get_api_object();    
  $ig_min = _infogentsia_installer_load_text('Infogentsia_minimal.html');
  if ($ig_min) {    
    $response = $mcapi->templates->add('Infogentsia_minimal_' . _infogentsia_installer_password_gen(8),$ig_min);    
    if (isset($response['template_id'])) {
        variable_set('infogentsia_newsletters_template_id', $response['template_id']);
    } else {
        drupal_set_message('Error adding Infogentsia Minimal template to MailChimp. Please contact Infogentsia.','error');
    }
  }

  // Update signup
  $list = variable_get('infogentsia_newsletters_list','');
  $num_updated = db_update('mailchimp_signup') 
  ->fields(array(
    'mc_lists' => serialize(array($list=>$list)),
  ))
  ->execute();  

  $account = user_load('2');
  // Register the user if not currently registered
  $merge_vars = array();
  $email = array('email' => $account->mail);
  $groupings = array('id' => variable_get('infogentsia_newsletters_interest_list_group',''),'groups' => array('All Topics'));
  $merge_vars['groupings'] = array($groupings);
  // Get list of subscribed groups
  // subscribe($id, $email, $merge_vars=null, $email_type='html', $double_optin=true, $update_existing=false, $replace_interests=true, $send_welcome=false) {
  $register = $mcapi->lists->subscribe(variable_get('infogentsia_newsletters_list',''), $email, $merge_vars, 'html', FALSE, TRUE, FALSE, TRUE);
  if (array_key_exists('error', $register)) {
    drupal_set_message('There was an error creating the subscriber','error');
    watchdog('infogentsia_newsletters', 'Unable to create subscription on list @list for user @user. Error: "%message"', array('@list' => variable_get('infogentsia_newsletters_list',''), '@user' => $account->mail, '%message' => $e->getMessage(),), WATCHDOG_ERROR); 
  } else {
    drupal_set_message('You are now subscribed to All Topics.');
  }

  // Since it takes a while for the user to get subscribed to the list, campaign will be created in last step
  if ($form_state['values']['infogentsia_newsletters_start_campaign'] == 1) variable_set('infogentsia_newsletters_start_campaign','1');
}
