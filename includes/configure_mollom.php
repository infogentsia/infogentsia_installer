<?php
/*
 * CONFIGURE MOLLOM
 */
function _infogentsia_installer_mollom() {
  drupal_set_title(t('Configure Mollom'));
  $form['instructions'] = array(
    '#markup' => t(_infogentsia_installer_load_text('mollom.txt'))
  );
  $form['mollom_public_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Mollom Public Key'),
    '#description' => t('The Mollom Public key to be used with this site.'),
    '#default_value' => variable_get('mollom_public_key',''),
    '#maxlength' => 72,
    '#required' => FALSE,
    '#size' => 32
  );
  $form['mollom_private_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Mollom Private Key'),
    '#description' => t('The Mollom Private key to be used with this site.'),
    '#default_value' => variable_get('mollom_private_key',''),
    '#maxlength' => 72,
    '#required' => FALSE,
    '#size' => 32
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Mollom Keys'),
  );
  $form['skip'] = array(
    '#type' => 'submit',
    '#value' => t('Skip Mollom Configuration'),
  );
  return $form;
}
function _infogentsia_installer_mollom_submit($form, &$form_state) {
  if ($form_state['values']['submit'] == t('Save Mollom Keys') && $form_state['values']['mollom_private_key'] != '' && $form_state['values']['mollom_public_key'] != '') {

    module_enable(array('mollom'));
    drupal_set_message(t('Enabled Mollom. You now automated content moderation.'));

    $mollom_private_key = trim($form_state['values']['mollom_private_key']);
    $mollom_public_key =  trim($form_state['values']['mollom_public_key']);
    variable_set('mollom_private_key', $mollom_private_key);
    variable_set('mollom_public_key',  $mollom_public_key);
    drupal_set_message(t('Mollom keys saved.'));

    // Set Mollom permissions
    $roles = user_roles();
    foreach (array('administrator','Content Manager','Feed Admin','Site Manager') as $role) {
      $rid = array_search($role,$roles);
      user_role_grant_permissions($rid, array('report to mollom'));
    }

    drupal_set_message(t('Mollom permissions set.'));
  }
}