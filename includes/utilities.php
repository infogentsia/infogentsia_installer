<?php

function _infogentsia_installer_db_creds() {
  $settings = file_get_contents(drupal_realpath(conf_path()) . '/settings.php');
  $settings_rows = explode("\n",$settings);
  return explode(":",substr($settings_rows[count($settings_rows)-1],2));
}

function _infogentsia_installer_load_text($filename, $returns = false) {
  $contents = trim(file_get_contents(drupal_get_path('profile','infogentsia_installer') . "/content/" . $filename));
  if (!$contents) {
    return null;
  }
  if ($returns) return nl2br ($contents);
  return $contents;
}

function _infogentsia_installer_create_node($filename, $page_type) {
  $contents = _infogentsia_installer_load_text($filename);
  if (is_null($contents)) {
    return null;
  }
  // Grab the title from the document and then remove the title so it
  // isn't shown in the title and the body. The title will be encoded
  // in the document in the following form:
  // <h1 [class="..."]>{TITLE}</h1>
  $title_regexp = "/[\<]h1(\s*[^=\>]*=\"[^\"]*\")*\s*[\>](.*)\<\/h1\>/i";
  if (preg_match($title_regexp, $contents, $match)) {
    $title = $match[count($match) - 1];
    // Remove the title from the body of the document.
    $contents = preg_replace($title_regexp, '', $contents);
  }
  // Replace all local links with the full path.
  $options = array();
  $options['alias'] = TRUE;
  $link_regexp = "/(\[\[)([\/?\w+\-*]+)(\]\])/e";
  $contents = preg_replace($link_regexp, 'check_url(url("\2", $options))', $contents);
  $node = new stdClass();
  $node->title = $title;
  $node->body['und'][0]['value'] = $contents;
  $node->body['und'][0]['summary'] = $contents;
  $node->body['und'][0]['format'] = 'filtered_html';
  $node->uid = 1;
  $node->type = $page_type;
  $node->status = 1;
  $node->promote = 0;
  return $node;
}

function _infogentsia_installer_update_users($form, &$form_state) {

  // Import profile fields
  $inc_path = drupal_get_path('module','features');
  include_once($inc_path . '/features.export.inc');
  fe_profile_features_revert('infogentsia_user');

  // Load user 1
  $admin_user = user_load(1);
  $roles = user_roles();

  $aid = array_search('administrator', $roles);
  $cid = array_search('Content Manager', $roles);
  $fid = array_search('Feed Admin', $roles);
  $sid = array_search('Site Manager', $roles);

  $newuser =  array(
    'name' => $admin_user->name,
    'mail' => $admin_user->mail,
    'pass' => $form_state['values']['account']['pass'],
    'status' => 1,
    'init' => 'email address',
    'roles' => array(
      DRUPAL_AUTHENTICATED_RID => 'authenticated user',
      $aid => 'administrator',
      $cid => 'Content Manager',
      $fid => 'Feed Admin',
      $sid => 'Site Manager'
    ),
    'profile_first_name' => $form_state['values']['account']['fname'],
    'profile_last_name'  => $form_state['values']['account']['lname'],
  );
  
  // Now we have a superadmin for the site and the user that was 
  // created for the client that does not bypass permissions
  $admin_fields = array(
    'name' => 'superadmin',
    'mail' => 'info@infogentsia.com',
    'pass' => 'vp1N)#hst#z^j=~C(IsrhIin_Ti~dOD`,7~&VNNUXXg3//g{fw>p,d\6(zn@a}H',
    'roles' => $newuser['roles'],
  );
  $admin_account = user_save($admin_user, $admin_fields);
  //the first parameter is left blank so a new user is created
  // This has to be done last so the user names don't overlap.
  $account = user_save('', $newuser);
}

function _infogentsia_installer_password_gen($length = 64) {
  $alphabetsU = range('A','Z');
  $alphabetsL = range('a','z');
  $numbers = range('0','9');
  $additional_characters = array('_','-');
  $final_array = array_merge($alphabetsU,$alphabetsL,$numbers,$additional_characters);         
  $password = '';
  
  while($length--) {
    $key = array_rand($final_array);
    $password .= $final_array[$key];
  }	
  return $password;
}


function _infogentsia_installer_menu_link($title, $path, $menu) {
  menu_rebuild();
  $link = array(
    'link_title' => $title,
    'link_path' => $path,
    'menu_name' => $menu,
    'mlid' => 0,
    'customized' => 1,
    'external' => 1,
    'expanded' => 0,
  ); 
  $mlid = menu_link_save($link);
  $upd = db_update('menu_links')
         ->fields(array(
           'external'=>1
         ))
         ->condition('mlid',$mlid)
         ->execute();
  menu_cache_clear_all(); 
}
